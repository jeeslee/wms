package org.jeecg.modules.wms.stockIn.service;

import org.jeecg.modules.wms.stockIn.entity.WmsStockIn;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 已上架表
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface IWmsStockInService extends IService<WmsStockIn> {

    void onShelfChange(WmsStockIn wmsStockIn);
}
