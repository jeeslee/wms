package org.jeecg.modules.wms.auditIn.service;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.wms.auditIn.entity.WmsAuditIn;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 待上架表
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface IWmsAuditInService extends IService<WmsAuditIn> {

    void onShelf(WmsAuditIn wmsAuditIn);
}
