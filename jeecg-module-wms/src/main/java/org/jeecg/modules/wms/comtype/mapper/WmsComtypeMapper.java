package org.jeecg.modules.wms.comtype.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.wms.comtype.entity.WmsComtype;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 企业类型
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface WmsComtypeMapper extends BaseMapper<WmsComtype> {

}
