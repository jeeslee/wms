package org.jeecg.modules.wms.goods.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.wms.goods.entity.WmsGoods;
import org.jeecg.modules.wms.goods.service.IWmsGoodsService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.authz.annotation.RequiresPermissions;

 /**
 * @Description: 物料
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Api(tags="物料")
@RestController
@RequestMapping("/goods/wmsGoods")
@Slf4j
public class WmsGoodsController extends JeecgController<WmsGoods, IWmsGoodsService> {
	@Autowired
	private IWmsGoodsService wmsGoodsService;
	
	/**
	 * 分页列表查询
	 *
	 * @param wmsGoods
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "物料-分页列表查询")
	@ApiOperation(value="物料-分页列表查询", notes="物料-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<WmsGoods>> queryPageList(WmsGoods wmsGoods,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<WmsGoods> queryWrapper = QueryGenerator.initQueryWrapper(wmsGoods, req.getParameterMap());
		Page<WmsGoods> page = new Page<WmsGoods>(pageNo, pageSize);
		IPage<WmsGoods> pageList = wmsGoodsService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param wmsGoods
	 * @return
	 */
	@AutoLog(value = "物料-添加")
	@ApiOperation(value="物料-添加", notes="物料-添加")
	@RequiresPermissions("goods:wms_goods:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody WmsGoods wmsGoods) {
		return wmsGoodsService.saveGoods(wmsGoods);
	}
	
	/**
	 *  编辑
	 *
	 * @param wmsGoods
	 *
	 * @return
	 */
	@AutoLog(value = "物料-编辑")
	@ApiOperation(value="物料-编辑", notes="物料-编辑")
	@RequiresPermissions("goods:wms_goods:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody WmsGoods wmsGoods) {

		return wmsGoodsService.updateGoods(wmsGoods);
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "物料-通过id删除")
	@ApiOperation(value="物料-通过id删除", notes="物料-通过id删除")
	@RequiresPermissions("goods:wms_goods:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		wmsGoodsService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "物料-批量删除")
	@ApiOperation(value="物料-批量删除", notes="物料-批量删除")
	@RequiresPermissions("goods:wms_goods:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.wmsGoodsService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "物料-通过id查询")
	@ApiOperation(value="物料-通过id查询", notes="物料-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<WmsGoods> queryById(@RequestParam(name="id",required=true) String id) {
		WmsGoods wmsGoods = wmsGoodsService.getById(id);
		if(wmsGoods==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(wmsGoods);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param wmsGoods
    */
    @RequiresPermissions("goods:wms_goods:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, WmsGoods wmsGoods) {
        return super.exportXls(request, wmsGoods, WmsGoods.class, "物料");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequiresPermissions("goods:wms_goods:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, WmsGoods.class);
    }

}
