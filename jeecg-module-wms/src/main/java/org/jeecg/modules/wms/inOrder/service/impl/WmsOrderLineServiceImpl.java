package org.jeecg.modules.wms.inOrder.service.impl;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.NumberUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.modules.wms.auditIn.entity.WmsAuditIn;
import org.jeecg.modules.wms.auditIn.mapper.WmsAuditInMapper;
import org.jeecg.modules.wms.common.constant.CommonWms;
import org.jeecg.modules.wms.common.service.IWmsGetCommonServiceImpl;
import org.jeecg.modules.wms.inOrder.entity.WmsOrderHeader;
import org.jeecg.modules.wms.inOrder.entity.WmsOrderLine;
import org.jeecg.modules.wms.inOrder.mapper.WmsOrderHeaderMapper;
import org.jeecg.modules.wms.inOrder.mapper.WmsOrderLineMapper;
import org.jeecg.modules.wms.inOrder.service.IWmsOrderLineService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description: 入库单子表
 * @Author: jeecg-boot
 * @Date: 2023-11-13
 * @Version: V1.0
 */
@Service
public class WmsOrderLineServiceImpl extends ServiceImpl<WmsOrderLineMapper, WmsOrderLine> implements IWmsOrderLineService {

    @Autowired
    private WmsOrderLineMapper wmsOrderLineMapper;
    @Autowired
    private WmsOrderHeaderMapper wmsOrderHeaderMapper;
    @Autowired
    private WmsAuditInMapper wmsAuditInMapper;
    @Autowired
    private IWmsGetCommonServiceImpl getCommonService;


    @Override
    public List<WmsOrderLine> selectByMainId(String mainId) {
        return wmsOrderLineMapper.selectByMainId(mainId);
    }

    @Override
    @Transactional
    public void qualityInspec(WmsOrderLine wmsOrderLinevo) {
        WmsOrderLine wmsOrderLine = wmsOrderLineMapper.selectById(wmsOrderLinevo.getId());
        if (wmsOrderLinevo.getGoodsCount().equals(wmsOrderLine.getGoodsDjCount())) {
            throw new JeecgBootException("物料已收完，不允许再次收货");
        } else {
            String goodsCount = wmsOrderLinevo.getGoodsCount();
            String goodsYsCount = wmsOrderLinevo.getGoodsYsCount();
            if (Convert.toInt(NumberUtil.sub(goodsYsCount, wmsOrderLinevo.getGoodsSyCount())) > 0) {
                throw new JeecgBootException("验收数量不能大于剩余数量");
            }

            //生成上架任务
            WmsAuditIn auditIn = new WmsAuditIn();
            auditIn.setOrderNum(wmsOrderLine.getOrderNum());
            auditIn.setOrderLineId(wmsOrderLine.getId());
            auditIn.setGoodsCode(wmsOrderLine.getGoodsCode());
            auditIn.setGoodsName(wmsOrderLine.getGoodsName());
            auditIn.setAuditCount(wmsOrderLinevo.getGoodsYsCount());
            auditIn.setShelfDjCount("0");
            auditIn.setShelfSyCount(wmsOrderLinevo.getGoodsYsCount());
            auditIn.setGoodsProDate(wmsOrderLinevo.getGoodsProDate());
            auditIn.setBinCode(wmsOrderLinevo.getBinCode());
            auditIn.setTrayCode(wmsOrderLinevo.getTrayCode());
            auditIn.setGoodsUnitCode(wmsOrderLine.getGoodsUnitCode());
            auditIn.setGoodsUnitName(wmsOrderLine.getGoodsUnitName());
            auditIn.setGoodsBatch(wmsOrderLine.getGoodsBatch());
            auditIn.setKwCode(wmsOrderLine.getKwPlanCode());
            auditIn.setShelfStatus(CommonWms.WAIT_FOR_SHELF);
            auditIn.setCusCode(wmsOrderLine.getCusCode());
            auditIn.setQcMark(wmsOrderLinevo.getQcMark());
            auditIn.setCusName(getCommonService.getName(wmsOrderLine.getCusCode(), CommonWms.CUS_CODE));
            wmsAuditInMapper.insert(auditIn);

            //验收数量计算
            wmsOrderLinevo.setGoodsDjCount(Convert.toStr(NumberUtil.add(goodsYsCount, wmsOrderLine.getGoodsDjCount())));
            wmsOrderLinevo.setGoodsSyCount(Convert.toStr(NumberUtil.sub(goodsCount, Convert.toStr(NumberUtil.add(goodsYsCount, wmsOrderLine.getGoodsDjCount())))));
            wmsOrderLinevo.setGoodsYsCount("");
            wmsOrderLinevo.setBinCode("");
            wmsOrderLinevo.setTrayCode("");

            //单据状态修改
            WmsOrderHeader wmsOrderHeader = wmsOrderHeaderMapper.selectOne(new LambdaQueryWrapper<WmsOrderHeader>().eq(WmsOrderHeader::getOrderNum, wmsOrderLine.getOrderNum()));
            if (wmsOrderLinevo.getGoodsDjCount().equals(wmsOrderLinevo.getGoodsCount())) {
                wmsOrderLinevo.setLineStatus(CommonWms.END_FOR_CHECK);
                wmsOrderLineMapper.updateById(wmsOrderLinevo);
                if (wmsOrderHeader != null) {
                    List<WmsOrderLine> orderLines = this.baseMapper.selectList(Wrappers.<WmsOrderLine>lambdaQuery()
                            .eq(WmsOrderLine::getOrderNum, wmsOrderLine.getOrderNum()));
                    List<WmsOrderLine> receivedList = orderLines.stream().filter(line -> line.getLineStatus().equals(CommonWms.END_FOR_CHECK)).collect(Collectors.toList());
                    if (orderLines.size() == receivedList.size()) {
                        wmsOrderHeader.setOrderStatus(CommonWms.END_FOR_CHECK);
                        wmsOrderHeaderMapper.updateById(wmsOrderHeader);
                    }
                }
            } else {
                wmsOrderLinevo.setLineStatus(CommonWms.BEGIN_FOR_CHECK);
                wmsOrderLineMapper.updateById(wmsOrderLinevo);
                if (wmsOrderHeader != null) {
                    wmsOrderHeader.setOrderStatus(CommonWms.BEGIN_FOR_CHECK);
                    wmsOrderHeaderMapper.updateById(wmsOrderHeader);
                }
            }
        }
    }
}
