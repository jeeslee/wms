package org.jeecg.modules.wms.outOrder.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.wms.outOrder.entity.WmsOutOrderHeader;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 出库单主表
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface WmsOutOrderHeaderMapper extends BaseMapper<WmsOutOrderHeader> {

}
