package org.jeecg.modules.wms.goods.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.wms.common.constant.CommonWms;
import org.jeecg.modules.wms.common.service.IWmsGetCommonServiceImpl;
import org.jeecg.modules.wms.goods.entity.WmsGoods;
import org.jeecg.modules.wms.goods.mapper.WmsGoodsMapper;
import org.jeecg.modules.wms.goods.service.IWmsGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

/**
 * @Description: 物料
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Service
public class WmsGoodsServiceImpl extends ServiceImpl<WmsGoodsMapper, WmsGoods> implements IWmsGoodsService {


    @Autowired
    private IWmsGetCommonServiceImpl wmsGetNameService;

    @Override
    public Result saveGoods(WmsGoods wmsGoods) {
        setWmsGoodsName(wmsGoods);
        List<WmsGoods> goods = this.list(new LambdaQueryWrapper<WmsGoods>().eq(WmsGoods::getGoodsCode, wmsGoods.getGoodsCode()));
        if (CollUtil.isNotEmpty(goods)){
            WmsGoods wmsGoods1 = goods.get(0);
            BeanUtil.copyProperties(wmsGoods,wmsGoods1, "id");
            return Result.ok(this.updateById(wmsGoods1));
        }
        return Result.ok(this.save(wmsGoods));
    }

    @Override
    public Result updateGoods(WmsGoods wmsGoods) {
        setWmsGoodsName(wmsGoods);
        return Result.ok(this.updateById(wmsGoods));
    }

    //组装欠缺名称
    private void setWmsGoodsName(WmsGoods wmsGoods) {
        wmsGoods.setCusName(wmsGetNameService.getName(wmsGoods.getCusCode(), CommonWms.CUS_CODE));
        wmsGoods.setUnitName(wmsGetNameService.getName(wmsGoods.getUnitCode(), CommonWms.UNIT_CODE));
        wmsGoods.setGoodsTypeName(wmsGetNameService.getName(wmsGoods.getGoodsTypeCode(), CommonWms.GOODS_TYPE_CODE));
    }

}
