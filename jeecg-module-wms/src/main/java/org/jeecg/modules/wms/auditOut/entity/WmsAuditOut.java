package org.jeecg.modules.wms.auditOut.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 待下架表
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Data
@TableName("wms_audit_out")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="wms_audit_out对象", description="待下架表")
public class WmsAuditOut implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
	/**创建人登录名称*/
    @ApiModelProperty(value = "创建人登录名称")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人登录名称*/
    @ApiModelProperty(value = "更新人登录名称")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**所属公司*/
	@Excel(name = "所属公司", width = 15)
    @ApiModelProperty(value = "所属公司")
    private String sysCompanyCode;
	/**单号*/
	@Excel(name = "单号", width = 15)
    @ApiModelProperty(value = "单号")
    private String orderNum;
	/**行项目*/
	@Excel(name = "行项目", width = 15)
    @ApiModelProperty(value = "行项目")
    private String lineOrderId;
	/**物料编码*/
	@Excel(name = "物料编码", width = 15)
    @ApiModelProperty(value = "物料编码")
    private String goodsCode;
	/**确认数量*/
	@Excel(name = "确认数量", width = 15)
    @ApiModelProperty(value = "确认数量")
    private String goodsQua;
    /**缺货数量*/
    @Excel(name = "缺货数量", width = 15)
    @ApiModelProperty(value = "缺货数量")
    private String outStockQua;
	/**已下架数量*/
	@Excel(name = "已下架数量", width = 15)
    @ApiModelProperty(value = "已下架数量")
    private String shelfOkQua;
	/**待下架数量*/
	@Excel(name = "待下架数量", width = 15)
    @ApiModelProperty(value = "待下架数量")
    private String shelfSyQua;
    /**本次下架数量*/
    @TableField(exist = false)
    private String shelfOffQua;
    /**生产日期*/
    @Excel(name = "生产日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "生产日期")
    private Date goodsProDate;
	/**标签号*/
	@Excel(name = "标签号", width = 15)
    @ApiModelProperty(value = "标签号")
    private String binCode;
	/**托盘*/
	@Excel(name = "托盘", width = 15)
    @ApiModelProperty(value = "托盘")
    private String trayCode;
    /**单位编码*/
    @Excel(name = "单位编码", width = 15)
    @ApiModelProperty(value = "单位编码")
    private String goodsUnitCode;
    /**单位名称*/
    @Excel(name = "单位名称", width = 15)
    @ApiModelProperty(value = "单位名称")
    private String goodsUnitName;
	/**批次*/
	@Excel(name = "批次", width = 15)
    @ApiModelProperty(value = "批次")
    private String goodsBatch;
	/**仓位*/
	@Excel(name = "仓位", width = 15)
    @ApiModelProperty(value = "仓位")
    private String kwCode;
	/**是否已下架*/
	@Excel(name = "是否已下架", width = 15)
    @ApiModelProperty(value = "是否已下架")
    private String orderStatus;
	/**客户编码*/
	@Excel(name = "客户编码", width = 15)
    @ApiModelProperty(value = "客户编码")
    private String cusCode;
	/**客户名称*/
	@Excel(name = "客户名称", width = 15)
    @ApiModelProperty(value = "客户名称")
    private String cusName;
	/**物料名称*/
	@Excel(name = "物料名称", width = 15)
    @ApiModelProperty(value = "物料名称")
    private String goodsName;
	/**波次号*/
	@Excel(name = "波次号", width = 15)
    @ApiModelProperty(value = "波次号")
    private String waveCode;
	/**租户ID*/
	@Excel(name = "租户ID", width = 15)
    @ApiModelProperty(value = "租户ID")
    private Integer tenantId;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private String remark;
	/**删除状态(0-正常,1-已删除)*/
	@Excel(name = "删除状态(0-正常,1-已删除)", width = 15)
    @ApiModelProperty(value = "删除状态(0-正常,1-已删除)")
    @TableLogic
    private Integer delFlag;
}
