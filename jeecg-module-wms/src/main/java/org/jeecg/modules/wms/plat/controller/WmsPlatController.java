package org.jeecg.modules.wms.plat.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.wms.plat.entity.WmsPlat;
import org.jeecg.modules.wms.plat.service.IWmsPlatService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.authz.annotation.RequiresPermissions;

 /**
 * @Description: 月台
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Api(tags="月台")
@RestController
@RequestMapping("/plat/wmsPlat")
@Slf4j
public class WmsPlatController extends JeecgController<WmsPlat, IWmsPlatService> {
	@Autowired
	private IWmsPlatService wmsPlatService;
	
	/**
	 * 分页列表查询
	 *
	 * @param wmsPlat
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "月台-分页列表查询")
	@ApiOperation(value="月台-分页列表查询", notes="月台-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<WmsPlat>> queryPageList(WmsPlat wmsPlat,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<WmsPlat> queryWrapper = QueryGenerator.initQueryWrapper(wmsPlat, req.getParameterMap());
		Page<WmsPlat> page = new Page<WmsPlat>(pageNo, pageSize);
		IPage<WmsPlat> pageList = wmsPlatService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param wmsPlat
	 * @return
	 */
	@AutoLog(value = "月台-添加")
	@ApiOperation(value="月台-添加", notes="月台-添加")
	@RequiresPermissions("plat:wms_plat:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody WmsPlat wmsPlat) {
		wmsPlatService.save(wmsPlat);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param wmsPlat
	 * @return
	 */
	@AutoLog(value = "月台-编辑")
	@ApiOperation(value="月台-编辑", notes="月台-编辑")
	@RequiresPermissions("plat:wms_plat:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody WmsPlat wmsPlat) {
		wmsPlatService.updateById(wmsPlat);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "月台-通过id删除")
	@ApiOperation(value="月台-通过id删除", notes="月台-通过id删除")
	@RequiresPermissions("plat:wms_plat:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		wmsPlatService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "月台-批量删除")
	@ApiOperation(value="月台-批量删除", notes="月台-批量删除")
	@RequiresPermissions("plat:wms_plat:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.wmsPlatService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "月台-通过id查询")
	@ApiOperation(value="月台-通过id查询", notes="月台-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<WmsPlat> queryById(@RequestParam(name="id",required=true) String id) {
		WmsPlat wmsPlat = wmsPlatService.getById(id);
		if(wmsPlat==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(wmsPlat);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param wmsPlat
    */
    @RequiresPermissions("plat:wms_plat:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, WmsPlat wmsPlat) {
        return super.exportXls(request, wmsPlat, WmsPlat.class, "月台");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequiresPermissions("plat:wms_plat:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, WmsPlat.class);
    }

}
