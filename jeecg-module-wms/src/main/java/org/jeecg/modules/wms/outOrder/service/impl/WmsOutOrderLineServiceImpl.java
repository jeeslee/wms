package org.jeecg.modules.wms.outOrder.service.impl;

import java.util.Date;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.jeecg.modules.wms.auditOut.entity.WmsAuditOut;
import org.jeecg.modules.wms.auditOut.mapper.WmsAuditOutMapper;
import org.jeecg.modules.wms.auditOut.service.IWmsAuditOutService;
import org.jeecg.modules.wms.common.constant.CommonWms;
import org.jeecg.modules.wms.inOrder.entity.WmsOrderHeader;
import org.jeecg.modules.wms.inOrder.entity.WmsOrderLine;
import org.jeecg.modules.wms.outOrder.entity.WmsOutOrderHeader;
import org.jeecg.modules.wms.outOrder.entity.WmsOutOrderLine;
import org.jeecg.modules.wms.outOrder.mapper.WmsOutOrderHeaderMapper;
import org.jeecg.modules.wms.outOrder.mapper.WmsOutOrderLineMapper;
import org.jeecg.modules.wms.outOrder.service.IWmsOutOrderLineService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 出库单子表
 * @Author: jeecg-boot
 * @Date: 2023-11-13
 * @Version: V1.0
 */
@Service
public class WmsOutOrderLineServiceImpl extends ServiceImpl<WmsOutOrderLineMapper, WmsOutOrderLine> implements IWmsOutOrderLineService {

    @Autowired
    private WmsOutOrderLineMapper wmsOutOrderLineMapper;

    @Autowired
    private WmsOutOrderHeaderMapper wmsOutOrderHeaderMapper;

    @Autowired
    private WmsAuditOutMapper wmsAuditOutMapper;

    @Override
    public List<WmsOutOrderLine> selectByMainId(String mainId) {
        return wmsOutOrderLineMapper.selectByMainId(mainId);
    }

    @Override
    public void confirmTask(WmsOutOrderLine wmsOutOrderLine) {
        wmsOutOrderLine.setOrderLineStatus(CommonWms.CONFIRM_FOR_OUT);
        WmsOutOrderLine wmsOutOrderLineOld = wmsOutOrderLineMapper.selectById(wmsOutOrderLine.getId());

        WmsAuditOut wmsAuditOut = new WmsAuditOut();
        wmsAuditOut.setOrderNum(wmsOutOrderLineOld.getOrderNum());
        wmsAuditOut.setLineOrderId(wmsOutOrderLine.getId());
        wmsAuditOut.setGoodsCode(wmsOutOrderLine.getGoodsCode());
        wmsAuditOut.setGoodsQua(wmsOutOrderLine.getGoodsQua());
        wmsAuditOut.setShelfOkQua("0");
        wmsAuditOut.setShelfSyQua(wmsOutOrderLine.getGoodsQua());
        wmsAuditOut.setGoodsProDate(wmsOutOrderLine.getGoodsProDate());
        wmsAuditOut.setBinCode(wmsOutOrderLine.getBinCode());
        wmsAuditOut.setTrayCode(wmsOutOrderLine.getTrayCode());
        wmsAuditOut.setGoodsUnitCode(wmsOutOrderLineOld.getGoodsUnitCode());
        wmsAuditOut.setGoodsUnitName(wmsOutOrderLineOld.getGoodsUnitName());
        wmsAuditOut.setGoodsBatch(wmsOutOrderLine.getGoodsBatch());
        wmsAuditOut.setKwCode(wmsOutOrderLine.getKwCode());
        wmsAuditOut.setOrderStatus(CommonWms.CONFIRM_FOR_OUT);
        wmsAuditOut.setCusCode(wmsOutOrderLineOld.getCusCode());
        wmsAuditOut.setCusName(wmsOutOrderLineOld.getCusName());
        wmsAuditOut.setGoodsName(wmsOutOrderLine.getGoodsName());
        wmsAuditOutMapper.insert(wmsAuditOut);

        wmsOutOrderLineMapper.updateById(wmsOutOrderLine);

        //修改单据状态
        WmsOutOrderHeader wmsOutOrderHeader = wmsOutOrderHeaderMapper.selectOne(new LambdaQueryWrapper<WmsOutOrderHeader>().eq(WmsOutOrderHeader::getOrderNum, wmsOutOrderLineOld.getOrderNum()));
        if (wmsOutOrderHeader != null) {
            List<WmsOutOrderLine> orderLines = this.baseMapper.selectList(Wrappers.<WmsOutOrderLine>lambdaQuery()
                    .eq(WmsOutOrderLine::getOrderNum, wmsOutOrderLineOld.getOrderNum()));
            List<WmsOutOrderLine> receivedList = orderLines.stream().filter(line -> line.getOrderLineStatus().equals(CommonWms.CONFIRM_FOR_OUT)).collect(Collectors.toList());
            if (orderLines.size() == receivedList.size()) {
                wmsOutOrderHeader.setOrderStatus(CommonWms.CONFIRM_FOR_OUT);
                wmsOutOrderHeaderMapper.updateById(wmsOutOrderHeader);
            }
        }
    }
}
