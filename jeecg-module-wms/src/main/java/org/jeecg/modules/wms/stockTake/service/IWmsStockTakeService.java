package org.jeecg.modules.wms.stockTake.service;

import org.jeecg.modules.wms.stockTake.entity.WmsStockTake;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 盘点单
 * @Author: jeecg-boot
 * @Date:   2023-12-21
 * @Version: V1.0
 */
public interface IWmsStockTakeService extends IService<WmsStockTake> {

    void stockEdit(WmsStockTake wmsStockTake);

    void adjustAccount(List<String> asList);
}
