package org.jeecg.modules.wms.cus.service;

import org.jeecg.modules.wms.cus.entity.WmsCus;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 客户
 * @Author: jeecg-boot
 * @Date:   2023-11-10
 * @Version: V1.0
 */
public interface IWmsCusService extends IService<WmsCus> {

}
