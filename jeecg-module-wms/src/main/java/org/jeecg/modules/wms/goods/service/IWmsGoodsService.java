package org.jeecg.modules.wms.goods.service;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.wms.goods.entity.WmsGoods;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 物料
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface IWmsGoodsService extends IService<WmsGoods> {

    Result saveGoods(WmsGoods wmsGoods);

    Result updateGoods(WmsGoods wmsGoods);
}
