package org.jeecg.modules.wms.kw.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.wms.kw.entity.WmsKw;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 库位
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface WmsKwMapper extends BaseMapper<WmsKw> {

}
