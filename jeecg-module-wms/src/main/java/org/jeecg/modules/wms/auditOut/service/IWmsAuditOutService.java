package org.jeecg.modules.wms.auditOut.service;

import org.jeecg.modules.wms.auditOut.entity.WmsAuditOut;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 待下架表
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface IWmsAuditOutService extends IService<WmsAuditOut> {

    void shelfOff(WmsAuditOut wmsAuditOut);
}
