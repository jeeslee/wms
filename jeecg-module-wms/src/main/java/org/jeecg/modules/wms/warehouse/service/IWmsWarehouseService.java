package org.jeecg.modules.wms.warehouse.service;

import org.jeecg.modules.wms.warehouse.entity.WmsWarehouse;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 仓库
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface IWmsWarehouseService extends IService<WmsWarehouse> {

}
