package org.jeecg.modules.wms.comtype.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.wms.comtype.entity.WmsComtype;
import org.jeecg.modules.wms.comtype.service.IWmsComtypeService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.authz.annotation.RequiresPermissions;

 /**
 * @Description: 企业类型
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Api(tags="企业类型")
@RestController
@RequestMapping("/comtype/wmsComtype")
@Slf4j
public class WmsComtypeController extends JeecgController<WmsComtype, IWmsComtypeService> {
	@Autowired
	private IWmsComtypeService wmsComtypeService;
	
	/**
	 * 分页列表查询
	 *
	 * @param wmsComtype
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "企业类型-分页列表查询")
	@ApiOperation(value="企业类型-分页列表查询", notes="企业类型-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<WmsComtype>> queryPageList(WmsComtype wmsComtype,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<WmsComtype> queryWrapper = QueryGenerator.initQueryWrapper(wmsComtype, req.getParameterMap());
		Page<WmsComtype> page = new Page<WmsComtype>(pageNo, pageSize);
		IPage<WmsComtype> pageList = wmsComtypeService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param wmsComtype
	 * @return
	 */
	@AutoLog(value = "企业类型-添加")
	@ApiOperation(value="企业类型-添加", notes="企业类型-添加")
	@RequiresPermissions("comtype:wms_comtype:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody WmsComtype wmsComtype) {
		wmsComtypeService.save(wmsComtype);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param wmsComtype
	 * @return
	 */
	@AutoLog(value = "企业类型-编辑")
	@ApiOperation(value="企业类型-编辑", notes="企业类型-编辑")
	@RequiresPermissions("comtype:wms_comtype:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody WmsComtype wmsComtype) {
		wmsComtypeService.updateById(wmsComtype);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "企业类型-通过id删除")
	@ApiOperation(value="企业类型-通过id删除", notes="企业类型-通过id删除")
	@RequiresPermissions("comtype:wms_comtype:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		wmsComtypeService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "企业类型-批量删除")
	@ApiOperation(value="企业类型-批量删除", notes="企业类型-批量删除")
	@RequiresPermissions("comtype:wms_comtype:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.wmsComtypeService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "企业类型-通过id查询")
	@ApiOperation(value="企业类型-通过id查询", notes="企业类型-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<WmsComtype> queryById(@RequestParam(name="id",required=true) String id) {
		WmsComtype wmsComtype = wmsComtypeService.getById(id);
		if(wmsComtype==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(wmsComtype);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param wmsComtype
    */
    @RequiresPermissions("comtype:wms_comtype:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, WmsComtype wmsComtype) {
        return super.exportXls(request, wmsComtype, WmsComtype.class, "企业类型");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequiresPermissions("comtype:wms_comtype:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, WmsComtype.class);
    }

}
