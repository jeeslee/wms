package org.jeecg.modules.wms.outOrder.service;

import org.jeecg.modules.wms.outOrder.entity.WmsOutOrderLine;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 出库单子表
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface IWmsOutOrderLineService extends IService<WmsOutOrderLine> {

	/**
	 * 通过主表id查询子表数据
	 *
	 * @param mainId 主表id
	 * @return List<WmsOutOrderLine>
	 */
	public List<WmsOutOrderLine> selectByMainId(String mainId);

	/**
	 * 任务确认
	 * @param wmsOutOrderLine
	 */
    void confirmTask(WmsOutOrderLine wmsOutOrderLine);
}
