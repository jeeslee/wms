package org.jeecg.modules.wms.goodsType.service.impl;

import org.jeecg.modules.wms.goodsType.entity.WmsGoodsType;
import org.jeecg.modules.wms.goodsType.mapper.WmsGoodsTypeMapper;
import org.jeecg.modules.wms.goodsType.service.IWmsGoodsTypeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 物料类型
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Service
public class WmsGoodsTypeServiceImpl extends ServiceImpl<WmsGoodsTypeMapper, WmsGoodsType> implements IWmsGoodsTypeService {

}
