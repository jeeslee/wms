package org.jeecg.modules.wms.goods.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 物料
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Data
@TableName("wms_goods")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="wms_goods对象", description="物料")
public class WmsGoods implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private String id;
	/**创建人登录名称*/
    @ApiModelProperty(value = "创建人登录名称")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人登录名称*/
    @ApiModelProperty(value = "更新人登录名称")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**所属公司*/
	@Excel(name = "所属公司", width = 15)
    @ApiModelProperty(value = "所属公司")
    private String sysCompanyCode;
	/**客户编码*/
	@Excel(name = "客户编码", width = 15)
    @ApiModelProperty(value = "客户编码")
    private String cusCode;
	/**客户名称*/
	@Excel(name = "客户名称", width = 15)
    @ApiModelProperty(value = "客户名称")
    private String cusName;
	/**物料名称*/
	@Excel(name = "物料名称", width = 15)
    @ApiModelProperty(value = "物料名称")
    private String goodsName;
	/**物料简称*/
	@Excel(name = "物料简称", width = 15)
    @ApiModelProperty(value = "物料简称")
    private String goodsSimpleName;
	/**物料编码*/
	@Excel(name = "物料编码", width = 15)
    @ApiModelProperty(value = "物料编码")
    private String goodsCode;
	/**物料规格*/
	@Excel(name = "物料规格", width = 15)
    @ApiModelProperty(value = "物料规格")
    private String goodsSpec;
	/**物料颜色*/
	@Excel(name = "物料颜色", width = 15)
    @ApiModelProperty(value = "物料颜色")
    private String goodsColor;
	/**单位编码*/
	@Excel(name = "单位编码", width = 15)
    @ApiModelProperty(value = "单位编码")
    private String unitCode;
    /**单位名称*/
    @Excel(name = "单位名称", width = 15)
    @ApiModelProperty(value = "单位名称")
    private String unitName;
	/**最小单位编码*/
	@Excel(name = "最小单位编码", width = 15)
    @ApiModelProperty(value = "最小单位编码")
    private String unitMixCode;
	/**保质期*/
    @Excel(name = "保质期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "保质期")
    private java.util.Date goodsExpirat;
    /**生产日期*/
    @Excel(name = "生产日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "生产日期")
    private Date goodsProDate;
	/**物料描述*/
	@Excel(name = "物料描述", width = 15)
    @ApiModelProperty(value = "物料描述")
    private String goodsDetail;
	/**供应商编码*/
	@Excel(name = "供应商编码", width = 15)
    @ApiModelProperty(value = "供应商编码")
    private String suplyCode;
	/**供应商名称*/
	@Excel(name = "供应商名称", width = 15)
    @ApiModelProperty(value = "供应商名称")
    private String suplyName;
	/**英文名称*/
	@Excel(name = "英文名称", width = 15)
    @ApiModelProperty(value = "英文名称")
    private String goodsEnName;
	/**库位类型编码*/
	@Excel(name = "库位类型编码", width = 15)
    @ApiModelProperty(value = "库位类型编码")
    private String areaTypeCode;
	/**库位类型名称*/
	@Excel(name = "库位类型名称", width = 15)
    @ApiModelProperty(value = "库位类型名称")
    private String areaTypeName;
	/**物料分类编码*/
	@Excel(name = "物料分类编码", width = 15)
    @ApiModelProperty(value = "物料分类编码")
    private String goodsTypeCode;
	/**物料分类名称*/
	@Excel(name = "物料分类名称", width = 15)
    @ApiModelProperty(value = "物料分类名称")
    private String goodsTypeName;
	/**库存上限*/
	@Excel(name = "库存上限", width = 15)
    @ApiModelProperty(value = "库存上限")
    private String upLimit;
	/**库存下限*/
	@Excel(name = "库存下限", width = 15)
    @ApiModelProperty(value = "库存下限")
    private String downLimit;
	/**预警天数*/
	@Excel(name = "预警天数", width = 15)
    @ApiModelProperty(value = "预警天数")
    private Integer warnDays;
	/**物料价格*/
	@Excel(name = "物料价格", width = 15)
    @ApiModelProperty(value = "物料价格")
    private BigDecimal goodsPrice;
	/**租户ID*/
	@Excel(name = "租户ID", width = 15)
    @ApiModelProperty(value = "租户ID")
    private Integer tenantId;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private String remark;
	/**删除状态(0-正常,1-已删除)*/
	@Excel(name = "删除状态(0-正常,1-已删除)", width = 15)
    @ApiModelProperty(value = "删除状态(0-正常,1-已删除)")
    @TableLogic
    private Integer delFlag;
}
