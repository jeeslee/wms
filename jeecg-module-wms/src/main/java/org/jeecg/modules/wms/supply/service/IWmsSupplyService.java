package org.jeecg.modules.wms.supply.service;

import org.jeecg.modules.wms.supply.entity.WmsSupply;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 供应商
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface IWmsSupplyService extends IService<WmsSupply> {

}
