package org.jeecg.modules.wms.kw.service.impl;

import org.jeecg.modules.wms.kw.entity.WmsKw;
import org.jeecg.modules.wms.kw.mapper.WmsKwMapper;
import org.jeecg.modules.wms.kw.service.IWmsKwService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 库位
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Service
public class WmsKwServiceImpl extends ServiceImpl<WmsKwMapper, WmsKw> implements IWmsKwService {

}
