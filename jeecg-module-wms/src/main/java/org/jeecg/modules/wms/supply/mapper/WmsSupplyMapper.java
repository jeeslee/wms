package org.jeecg.modules.wms.supply.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.wms.supply.entity.WmsSupply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 供应商
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface WmsSupplyMapper extends BaseMapper<WmsSupply> {

}
