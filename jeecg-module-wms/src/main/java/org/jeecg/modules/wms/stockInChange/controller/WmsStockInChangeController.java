package org.jeecg.modules.wms.stockInChange.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.wms.stockInChange.entity.WmsStockInChange;
import org.jeecg.modules.wms.stockInChange.service.IWmsStockInChangeService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.authz.annotation.RequiresPermissions;

 /**
 * @Description: 上架调整记录表
 * @Author: jeecg-boot
 * @Date:   2023-11-28
 * @Version: V1.0
 */
@Api(tags="上架调整记录表")
@RestController
@RequestMapping("/stockInChange/wmsStockInChange")
@Slf4j
public class WmsStockInChangeController extends JeecgController<WmsStockInChange, IWmsStockInChangeService> {
	@Autowired
	private IWmsStockInChangeService wmsStockInChangeService;
	
	/**
	 * 分页列表查询
	 *
	 * @param wmsStockInChange
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "上架调整记录表-分页列表查询")
	@ApiOperation(value="上架调整记录表-分页列表查询", notes="上架调整记录表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<WmsStockInChange>> queryPageList(WmsStockInChange wmsStockInChange,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<WmsStockInChange> queryWrapper = QueryGenerator.initQueryWrapper(wmsStockInChange, req.getParameterMap());
		Page<WmsStockInChange> page = new Page<WmsStockInChange>(pageNo, pageSize);
		IPage<WmsStockInChange> pageList = wmsStockInChangeService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param wmsStockInChange
	 * @return
	 */
	@AutoLog(value = "上架调整记录表-添加")
	@ApiOperation(value="上架调整记录表-添加", notes="上架调整记录表-添加")
	@RequiresPermissions("stockInChange:wms_stock_in_change:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody WmsStockInChange wmsStockInChange) {
		wmsStockInChangeService.save(wmsStockInChange);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param wmsStockInChange
	 * @return
	 */
	@AutoLog(value = "上架调整记录表-编辑")
	@ApiOperation(value="上架调整记录表-编辑", notes="上架调整记录表-编辑")
	@RequiresPermissions("stockInChange:wms_stock_in_change:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody WmsStockInChange wmsStockInChange) {
		wmsStockInChangeService.updateById(wmsStockInChange);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "上架调整记录表-通过id删除")
	@ApiOperation(value="上架调整记录表-通过id删除", notes="上架调整记录表-通过id删除")
	@RequiresPermissions("stockInChange:wms_stock_in_change:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		wmsStockInChangeService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "上架调整记录表-批量删除")
	@ApiOperation(value="上架调整记录表-批量删除", notes="上架调整记录表-批量删除")
	@RequiresPermissions("stockInChange:wms_stock_in_change:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.wmsStockInChangeService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "上架调整记录表-通过id查询")
	@ApiOperation(value="上架调整记录表-通过id查询", notes="上架调整记录表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<WmsStockInChange> queryById(@RequestParam(name="id",required=true) String id) {
		WmsStockInChange wmsStockInChange = wmsStockInChangeService.getById(id);
		if(wmsStockInChange==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(wmsStockInChange);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param wmsStockInChange
    */
    @RequiresPermissions("stockInChange:wms_stock_in_change:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, WmsStockInChange wmsStockInChange) {
        return super.exportXls(request, wmsStockInChange, WmsStockInChange.class, "上架调整记录表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequiresPermissions("stockInChange:wms_stock_in_change:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, WmsStockInChange.class);
    }

}
