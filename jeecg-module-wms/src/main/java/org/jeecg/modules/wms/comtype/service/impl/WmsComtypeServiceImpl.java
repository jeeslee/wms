package org.jeecg.modules.wms.comtype.service.impl;

import org.jeecg.modules.wms.comtype.entity.WmsComtype;
import org.jeecg.modules.wms.comtype.mapper.WmsComtypeMapper;
import org.jeecg.modules.wms.comtype.service.IWmsComtypeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 企业类型
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Service
public class WmsComtypeServiceImpl extends ServiceImpl<WmsComtypeMapper, WmsComtype> implements IWmsComtypeService {

}
