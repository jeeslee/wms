package org.jeecg.modules.wms.common.constant;

public interface CommonWms {

    //客户编码
    String  CUS_CODE  = "cusCode";

    //单位编码
    String  UNIT_CODE  = "unitCode";

    //物料分类编码
    String  GOODS_TYPE_CODE  = "goodsTypeCode";

    //供应商编码
    String  SUPLY_CODE  = "suplyCode";

    //订单类型名称
    String  ORDER_TYPE_CODE  = "orderTypeCode";

    //-------------收货状态----------------

    String  WAIT_FOR_CHECK  = "待验收";
    String  BEGIN_FOR_CHECK  = "验收中";
    String  END_FOR_CHECK  = "已验收";
    String  WAIT_FOR_SHELF  = "待上架";
    String  END_FOR_SHELF  = "已上架";
    String  BEGIN_FOR_SHELF  = "上架中";
    //------------------------------------
    //更改记录:上架调整等
    String  CHANGE_FOR_SHELF  = "上架调整";
    String  LABEL_ALLOCATION  = "标签转移";
    String  KW_ALLOCATION  = "库位转移";
    String  TRAY_ALLOCATION  = "托盘转移";


    //-------------出库状态----------------
    String  WAIT_FOR_OUT  = "待出库";
    String  CONFIRM_FOR_OUT  = "待下架";
    String  BIGIN_SHELF_OFF  = "下架中";
    String  FINISH_SHELF_OFF  = "已下架";
    String  OUT_OF_STOCK  = "库存不足";
    //------------------------------------

    //-------------盘点类型----------------
    String  CLOUSE_TAKE  = "暗盘";
    String  OPENLY_TAKE  = "明盘";
    String  TAKE_STATUS_DPD  = "待盘点";
    String  TAKE_STATUS_YWC  = "盘点完成";
    String  TAKE_STATUS_TZ  = "已调账";
    //------------------------------------

    //-------------合格标识----------------
    String  QC_GOOD_MARK  = "合格";
    String  QC_BAD_MARK  = "不合格";
    //------------------------------------

}
