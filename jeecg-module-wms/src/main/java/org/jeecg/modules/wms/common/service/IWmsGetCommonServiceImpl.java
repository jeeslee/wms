package org.jeecg.modules.wms.common.service;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.jeecg.modules.wms.common.constant.CommonWms;
import org.jeecg.modules.wms.cus.entity.WmsCus;
import org.jeecg.modules.wms.cus.service.IWmsCusService;
import org.jeecg.modules.wms.goodsType.entity.WmsGoodsType;
import org.jeecg.modules.wms.goodsType.service.IWmsGoodsTypeService;
import org.jeecg.modules.wms.orderType.entity.WmsOrderType;
import org.jeecg.modules.wms.orderType.service.IWmsOrderTypeService;
import org.jeecg.modules.wms.supply.entity.WmsSupply;
import org.jeecg.modules.wms.supply.service.IWmsSupplyService;
import org.jeecg.modules.wms.unit.entity.WmsUnit;
import org.jeecg.modules.wms.unit.service.IWmsUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IWmsGetCommonServiceImpl {

    @Autowired
    private IWmsCusService wmsCusService;
    @Autowired
    private IWmsUnitService wmsUnitService;
    @Autowired
    private IWmsGoodsTypeService wmsGoodsTypeService;
    @Autowired
    private IWmsSupplyService wmsSupplyService;
    @Autowired
    private IWmsOrderTypeService wmsOrderTypeService;

    /**
     * 根据编码获取对应名称
     * @param code 编码
     * @param field  条件
     * @return
     */
    public String getName(String code,String field) {
        switch (field){
            case CommonWms.CUS_CODE:
                List<WmsCus> cusList = wmsCusService.list(new LambdaQueryWrapper<WmsCus>().eq(WmsCus::getCusCode, code));
                if (CollUtil.isNotEmpty(cusList)){
                    return cusList.get(0).getCusName();
                }
                break;
            case CommonWms.UNIT_CODE:
                List<WmsUnit> units = wmsUnitService.list(new LambdaQueryWrapper<WmsUnit>().eq(WmsUnit::getUnitCode, code));
                if (CollUtil.isNotEmpty(units)){
                    return units.get(0).getUnitName();
                }
                break;
            case CommonWms.GOODS_TYPE_CODE:
                List<WmsGoodsType> goodsTypes = wmsGoodsTypeService.list(new LambdaQueryWrapper<WmsGoodsType>().eq(WmsGoodsType::getTypeCode, code));
                if (CollUtil.isNotEmpty(goodsTypes)){
                    return goodsTypes.get(0).getTypeName();
                }
                break;
            case CommonWms.SUPLY_CODE:
                List<WmsSupply> supplies = wmsSupplyService.list(new LambdaQueryWrapper<WmsSupply>().eq(WmsSupply::getSuplyCode, code));
                if (CollUtil.isNotEmpty(supplies)){
                    return supplies.get(0).getSuplyName();
                }
                break;
            case CommonWms.ORDER_TYPE_CODE:
                List<WmsOrderType> orderTypes = wmsOrderTypeService.list(new LambdaQueryWrapper<WmsOrderType>().eq(WmsOrderType::getOrderTypeCode, code));
                if (CollUtil.isNotEmpty(orderTypes)){
                    return orderTypes.get(0).getOrderTypeName();
                }
                break;
            default:
                System.out.println("请重新输入");
        }
        return "";
    }
}
