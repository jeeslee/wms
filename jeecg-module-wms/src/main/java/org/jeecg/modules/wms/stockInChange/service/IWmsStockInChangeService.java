package org.jeecg.modules.wms.stockInChange.service;

import org.jeecg.modules.wms.stockInChange.entity.WmsStockInChange;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 上架调整记录表
 * @Author: jeecg-boot
 * @Date:   2023-11-28
 * @Version: V1.0
 */
public interface IWmsStockInChangeService extends IService<WmsStockInChange> {

}
