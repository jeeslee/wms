package org.jeecg.modules.wms.kw.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 库位
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Data
@TableName("wms_kw")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="wms_kw对象", description="库位")
public class WmsKw implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**仓库代码*/
	@Excel(name = "仓库代码", width = 15)
    @ApiModelProperty(value = "仓库代码")
    private String wareCode;
	/**仓库描述*/
	@Excel(name = "仓库描述", width = 15)
    @ApiModelProperty(value = "仓库描述")
    private String wareName;
	/**库区描述*/
	@Excel(name = "库区描述", width = 15)
    @ApiModelProperty(value = "库区描述")
    private String areaName;
	/**库区编码*/
	@Excel(name = "库区编码", width = 15)
    @ApiModelProperty(value = "库区编码")
    private String areaCode;
	/**库位编码*/
	@Excel(name = "库位编码", width = 15)
    @ApiModelProperty(value = "库位编码")
    private String kwCode;
	/**库位名称*/
	@Excel(name = "库位名称", width = 15)
    @ApiModelProperty(value = "库位名称")
    private String kwName;
	/**库位类型编码*/
	@Excel(name = "库位类型编码", width = 15)
    @ApiModelProperty(value = "库位类型编码")
    private String kwTypeCode;
	/**简称*/
	@Excel(name = "简称", width = 15)
    @ApiModelProperty(value = "简称")
    private String shortName;
	/**容纳描述*/
	@Excel(name = "容纳描述", width = 15)
    @ApiModelProperty(value = "容纳描述")
    private String kwDesc;
	/**库位所在层数*/
	@Excel(name = "库位所在层数", width = 15)
    @ApiModelProperty(value = "库位所在层数")
    private Integer kwLevel;
	/**货架编码*/
	@Excel(name = "货架编码", width = 15)
    @ApiModelProperty(value = "货架编码")
    private String shelfCode;
	/**库位所在列数*/
	@Excel(name = "库位所在列数", width = 15)
    @ApiModelProperty(value = "库位所在列数")
    private Integer kwColumn;
	/**货架名称*/
	@Excel(name = "货架名称", width = 15)
    @ApiModelProperty(value = "货架名称")
    private String shelfName;
	/**库位状态*/
	@Excel(name = "库位状态", width = 15)
    @ApiModelProperty(value = "库位状态")
    private String kwStatus;
	/**长度*/
	@Excel(name = "长度", width = 15)
    @ApiModelProperty(value = "长度")
    private Integer length;
	/**宽度*/
	@Excel(name = "宽度", width = 15)
    @ApiModelProperty(value = "宽度")
    private Integer width;
	/**高度*/
	@Excel(name = "高度", width = 15)
    @ApiModelProperty(value = "高度")
    private Integer heigh;
	/**尺寸单位*/
	@Excel(name = "尺寸单位", width = 15)
    @ApiModelProperty(value = "尺寸单位")
    private String measureMent;
	/**租户ID*/
	@Excel(name = "租户ID", width = 15)
    @ApiModelProperty(value = "租户ID")
    private Integer tenantId;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private String remark;
	/**最大库存量*/
	@Excel(name = "最大库存量", width = 15)
    @ApiModelProperty(value = "最大库存量")
    private String maxStock;
	/**删除状态(0-正常,1-已删除)*/
	@Excel(name = "删除状态(0-正常,1-已删除)", width = 15)
    @ApiModelProperty(value = "删除状态(0-正常,1-已删除)")
    @TableLogic
    private Integer delFlag;
}
