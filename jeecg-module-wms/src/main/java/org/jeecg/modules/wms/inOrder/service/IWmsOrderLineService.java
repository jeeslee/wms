package org.jeecg.modules.wms.inOrder.service;

import org.jeecg.modules.wms.inOrder.entity.WmsOrderLine;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 入库单子表
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface IWmsOrderLineService extends IService<WmsOrderLine> {

	/**
	 * 通过主表id查询子表数据
	 *
	 * @param mainId 主表id
	 * @return List<WmsOrderLine>
	 */
	public List<WmsOrderLine> selectByMainId(String mainId);

	/**
	 * 验收并装箱
	 * @param wmsOrderLinevo
	 */
    void qualityInspec(WmsOrderLine wmsOrderLinevo);

}
