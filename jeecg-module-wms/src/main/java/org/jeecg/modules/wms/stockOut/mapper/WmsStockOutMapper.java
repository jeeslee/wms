package org.jeecg.modules.wms.stockOut.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.wms.stockOut.entity.WmsStockOut;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 已下架表
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface WmsStockOutMapper extends BaseMapper<WmsStockOut> {

}
