package org.jeecg.modules.wms.stockIn.service.impl;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.NumberUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.modules.wms.common.constant.CommonWms;
import org.jeecg.modules.wms.inventory.entity.WmsInventory;
import org.jeecg.modules.wms.inventory.mapper.WmsInventoryMapper;
import org.jeecg.modules.wms.stockIn.entity.WmsStockIn;
import org.jeecg.modules.wms.stockIn.mapper.WmsStockInMapper;
import org.jeecg.modules.wms.stockIn.service.IWmsStockInService;
import org.jeecg.modules.wms.stockInChange.entity.WmsStockInChange;
import org.jeecg.modules.wms.stockInChange.mapper.WmsStockInChangeMapper;
import org.jeecg.modules.wms.stockInChange.service.IWmsStockInChangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * @Description: 已上架表
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Service
public class WmsStockInServiceImpl extends ServiceImpl<WmsStockInMapper, WmsStockIn> implements IWmsStockInService {
    @Autowired
    private WmsStockInChangeMapper wmsStockInChangeMapper;
    @Autowired
    private WmsInventoryMapper wmsInventoryMapper;

    @Override
    @Transactional
    public void onShelfChange(WmsStockIn wmsStockIn) {
        WmsStockIn stockIn = this.baseMapper.selectById(wmsStockIn.getId());
        if (!wmsStockIn.getShelfDjCount().equals(stockIn.getShelfDjCount()) && Convert.toInt(wmsStockIn.getShelfDjCount()) > 0 ){
            //新增上架调整记录
            WmsStockInChange stockInChange = new WmsStockInChange();
            stockInChange.setNewBinCode(wmsStockIn.getBinCode());
            stockInChange.setOldBinCode(stockIn.getBinCode());
            stockInChange.setNewKwCode(wmsStockIn.getKwCode());
            stockInChange.setOldKwCode(stockIn.getKwCode());
            stockInChange.setNewTrayCode(wmsStockIn.getTrayCode());
            stockInChange.setOldTrayCode(stockIn.getTrayCode());
            stockInChange.setShelfNewCount(wmsStockIn.getShelfDjCount());
            stockInChange.setShelfOldCount(stockIn.getShelfDjCount());
            stockInChange.setGoodsCode(stockIn.getGoodsCode());
            stockInChange.setGoodsName(stockIn.getGoodsName());
            stockInChange.setOrderNum(stockIn.getOrderNum());
            stockInChange.setSourceId(stockIn.getId());
            stockInChange.setRemark(CommonWms.CHANGE_FOR_SHELF);
            wmsStockInChangeMapper.insert(stockInChange);

            BigDecimal sub = NumberUtil.sub(wmsStockIn.getShelfDjCount(),stockIn.getShelfDjCount());

            //仓库记录  getBinCode   getKwCode  getTrayCode  getGoodsCode
            WmsInventory wmsInventories = wmsInventoryMapper.selectOne(new LambdaQueryWrapper<WmsInventory>()
                    .eq(WmsInventory::getBinCode, stockIn.getBinCode())
                    .eq(WmsInventory::getKwCode, stockIn.getKwCode())
                    .eq(WmsInventory::getTrayCode, stockIn.getTrayCode())
                    .eq(WmsInventory::getCusCode, stockIn.getCusCode())
                    .eq(WmsInventory::getGoodsBatch, stockIn.getGoodsBatch())
                    .eq(WmsInventory::getGoodsProDate, stockIn.getGoodsProDate())
                    .eq(WmsInventory::getGoodsUnitCode, stockIn.getGoodsUnitCode())
                    .eq(WmsInventory::getGoodsCode, stockIn.getGoodsCode())
            );

            if (wmsInventories == null) {
                throw new JeecgBootException("仓库无数据：物料编码 "+stockIn.getGoodsCode()+"物料名称 "+stockIn.getGoodsName());
            }

            BigDecimal add = NumberUtil.add(wmsInventories.getBaseCount(), Convert.toStr(sub));
            wmsInventories.setBaseCount(Convert.toStr(add));
            wmsInventoryMapper.updateById(wmsInventories);

            stockIn.setShelfDjCount(wmsStockIn.getShelfDjCount());
            this.baseMapper.updateById(stockIn);
        }
    }
}
