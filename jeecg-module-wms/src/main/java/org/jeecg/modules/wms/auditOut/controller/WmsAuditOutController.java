package org.jeecg.modules.wms.auditOut.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.wms.auditOut.entity.WmsAuditOut;
import org.jeecg.modules.wms.auditOut.service.IWmsAuditOutService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.wms.common.constant.CommonWms;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.authz.annotation.RequiresPermissions;

 /**
 * @Description: 待下架表
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Api(tags="待下架表")
@RestController
@RequestMapping("/auditOut/wmsAuditOut")
@Slf4j
public class WmsAuditOutController extends JeecgController<WmsAuditOut, IWmsAuditOutService> {
	@Autowired
	private IWmsAuditOutService wmsAuditOutService;
	
	/**
	 * 分页列表查询
	 *
	 * @param wmsAuditOut
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "待下架表-分页列表查询")
	@ApiOperation(value="待下架表-分页列表查询", notes="待下架表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<WmsAuditOut>> queryPageList(WmsAuditOut wmsAuditOut,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<WmsAuditOut> queryWrapper = QueryGenerator.initQueryWrapper(wmsAuditOut, req.getParameterMap());
		queryWrapper.in("order_status", CommonWms.CONFIRM_FOR_OUT,CommonWms.BIGIN_SHELF_OFF);
		Page<WmsAuditOut> page = new Page<WmsAuditOut>(pageNo, pageSize);
		IPage<WmsAuditOut> pageList = wmsAuditOutService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param wmsAuditOut
	 * @return
	 */
	@AutoLog(value = "待下架表-添加")
	@ApiOperation(value="待下架表-添加", notes="待下架表-添加")
	@RequiresPermissions("auditOut:wms_audit_out:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody WmsAuditOut wmsAuditOut) {
		wmsAuditOutService.save(wmsAuditOut);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  下架操作
	 *
	 * @param wmsAuditOut
	 * @return
	 */
	@AutoLog(value = "待下架表-下架操作")
	@ApiOperation(value="待下架表-下架操作", notes="待下架表-下架操作")
	@RequiresPermissions("auditOut:wms_audit_out:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody WmsAuditOut wmsAuditOut) {
		wmsAuditOutService.shelfOff(wmsAuditOut);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "待下架表-通过id删除")
	@ApiOperation(value="待下架表-通过id删除", notes="待下架表-通过id删除")
	@RequiresPermissions("auditOut:wms_audit_out:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		wmsAuditOutService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "待下架表-批量删除")
	@ApiOperation(value="待下架表-批量删除", notes="待下架表-批量删除")
	@RequiresPermissions("auditOut:wms_audit_out:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.wmsAuditOutService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "待下架表-通过id查询")
	@ApiOperation(value="待下架表-通过id查询", notes="待下架表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<WmsAuditOut> queryById(@RequestParam(name="id",required=true) String id) {
		WmsAuditOut wmsAuditOut = wmsAuditOutService.getById(id);
		if(wmsAuditOut==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(wmsAuditOut);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param wmsAuditOut
    */
    @RequiresPermissions("auditOut:wms_audit_out:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, WmsAuditOut wmsAuditOut) {
        return super.exportXls(request, wmsAuditOut, WmsAuditOut.class, "待下架表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequiresPermissions("auditOut:wms_audit_out:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, WmsAuditOut.class);
    }

}
