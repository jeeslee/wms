package org.jeecg.modules.wms.warehouseArea.service.impl;

import org.jeecg.modules.wms.warehouseArea.entity.WmsWarehouseArea;
import org.jeecg.modules.wms.warehouseArea.mapper.WmsWarehouseAreaMapper;
import org.jeecg.modules.wms.warehouseArea.service.IWmsWarehouseAreaService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 库区
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Service
public class WmsWarehouseAreaServiceImpl extends ServiceImpl<WmsWarehouseAreaMapper, WmsWarehouseArea> implements IWmsWarehouseAreaService {

}
