package org.jeecg.modules.wms.comtype.service;

import org.jeecg.modules.wms.comtype.entity.WmsComtype;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 企业类型
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface IWmsComtypeService extends IService<WmsComtype> {

}
