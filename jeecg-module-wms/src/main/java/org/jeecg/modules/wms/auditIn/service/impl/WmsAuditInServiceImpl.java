package org.jeecg.modules.wms.auditIn.service.impl;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.NumberUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.modules.wms.auditIn.entity.WmsAuditIn;
import org.jeecg.modules.wms.auditIn.mapper.WmsAuditInMapper;
import org.jeecg.modules.wms.auditIn.service.IWmsAuditInService;
import org.jeecg.modules.wms.common.constant.CommonWms;
import org.jeecg.modules.wms.inOrder.entity.WmsOrderHeader;
import org.jeecg.modules.wms.inOrder.entity.WmsOrderLine;
import org.jeecg.modules.wms.inOrder.mapper.WmsOrderHeaderMapper;
import org.jeecg.modules.wms.inOrder.mapper.WmsOrderLineMapper;
import org.jeecg.modules.wms.inventory.entity.WmsInventory;
import org.jeecg.modules.wms.inventory.mapper.WmsInventoryMapper;
import org.jeecg.modules.wms.inventory.service.IWmsInventoryService;
import org.jeecg.modules.wms.stockIn.entity.WmsStockIn;
import org.jeecg.modules.wms.stockIn.mapper.WmsStockInMapper;
import org.jeecg.modules.wms.stockIn.service.IWmsStockInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description: 待上架表
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Service
public class WmsAuditInServiceImpl extends ServiceImpl<WmsAuditInMapper, WmsAuditIn> implements IWmsAuditInService {
    @Autowired
    private WmsOrderLineMapper wmsOrderLineMapper;
    @Autowired
    private WmsOrderHeaderMapper wmsOrderHeaderMapper;
    @Autowired
    private WmsStockInMapper wmsStockInMapper;
    @Autowired
    private WmsInventoryMapper wmsInventoryMapper;

    @Override
    @Transactional
    public void onShelf(WmsAuditIn wmsAuditInvo) {
        WmsAuditIn auditIn = this.getById(wmsAuditInvo.getId());
        //验收数量
        String auditCount = auditIn.getAuditCount();
        //此次验收数量
        String ciAuditCount = wmsAuditInvo.getCiAuditCount();
        if (auditCount.equals(auditIn.getShelfDjCount())) {
            throw new JeecgBootException("物料已收完，不允许再次收货");
        } else{
            if (Convert.toInt(NumberUtil.sub(ciAuditCount,auditIn.getShelfSyCount()))>0){
                throw new JeecgBootException("上架数量不能大于上架剩余数量");
            }
            //验收数量计算
            wmsAuditInvo.setShelfDjCount(Convert.toStr(NumberUtil.add(ciAuditCount,auditIn.getShelfDjCount())));
            wmsAuditInvo.setShelfSyCount(Convert.toStr(NumberUtil.sub(auditCount,Convert.toStr(NumberUtil.add(ciAuditCount,auditIn.getShelfDjCount())))));

            //新增上架记录以及新增库存
            WmsStockIn wmsStockIn = new WmsStockIn();
            wmsStockIn.setGoodsCode(wmsAuditInvo.getGoodsCode());
            wmsStockIn.setGoodsName(wmsAuditInvo.getGoodsName());
            wmsStockIn.setShelfDjCount(wmsAuditInvo.getCiAuditCount());
            wmsStockIn.setOrderNum(auditIn.getOrderNum());
            wmsStockIn.setAuditId(auditIn.getId());
            wmsStockIn.setGoodsUnitCode(auditIn.getGoodsUnitCode());
            wmsStockIn.setGoodsUnitName(auditIn.getGoodsUnitName());
            wmsStockIn.setGoodsBatch(auditIn.getGoodsBatch());
            wmsStockIn.setGoodsProDate(auditIn.getGoodsProDate());
            wmsStockIn.setKwCode(wmsAuditInvo.getKwCode());
            wmsStockIn.setBinCode(wmsAuditInvo.getBinCode());
            wmsStockIn.setTrayCode(wmsAuditInvo.getTrayCode());
            wmsStockIn.setCusCode(auditIn.getCusCode());
            wmsStockIn.setCusName(auditIn.getCusName());
            wmsStockIn.setQcMark(auditIn.getQcMark());
            wmsStockInMapper.insert(wmsStockIn);

            WmsInventory wmsInventories = wmsInventoryMapper.selectOne(new LambdaQueryWrapper<WmsInventory>()
                    .eq(WmsInventory::getBinCode, wmsAuditInvo.getBinCode())
                    .eq(WmsInventory::getKwCode, wmsAuditInvo.getKwCode())
                    .eq(WmsInventory::getTrayCode, wmsAuditInvo.getTrayCode())
                    .eq(WmsInventory::getCusCode, wmsAuditInvo.getCusCode())
                    .eq(WmsInventory::getGoodsBatch, wmsAuditInvo.getGoodsBatch())
                    .eq(WmsInventory::getGoodsProDate, wmsAuditInvo.getGoodsProDate())
                    .eq(WmsInventory::getGoodsUnitCode, auditIn.getGoodsUnitCode())
                    .eq(WmsInventory::getGoodsCode, wmsAuditInvo.getGoodsCode())
                    .eq(WmsInventory::getQcMark, auditIn.getQcMark())
            );
            if (wmsInventories != null) {
                wmsInventories.setBaseCount(Convert.toStr(NumberUtil.add(wmsInventories.getBaseCount(),wmsAuditInvo.getCiAuditCount())));
                wmsInventoryMapper.updateById(wmsInventories);
            }else {
                WmsInventory wmsInventory = new WmsInventory();
                wmsInventory.setGoodsCode(wmsAuditInvo.getGoodsCode());
                wmsInventory.setGoodsName(wmsAuditInvo.getGoodsName());
                wmsInventory.setBaseCount(wmsAuditInvo.getCiAuditCount());
                wmsInventory.setGoodsUnitCode(auditIn.getGoodsUnitCode());
                wmsInventory.setGoodsUnitName(auditIn.getGoodsUnitName());
                wmsInventory.setGoodsBatch(auditIn.getGoodsBatch());
                wmsInventory.setGoodsProDate(auditIn.getGoodsProDate());
                wmsInventory.setKwCode(wmsAuditInvo.getKwCode());
                wmsInventory.setBinCode(wmsAuditInvo.getBinCode());
                wmsInventory.setTrayCode(wmsAuditInvo.getTrayCode());
                wmsInventory.setCusCode(auditIn.getCusCode());
                wmsInventory.setCusName(auditIn.getCusName());
                wmsInventory.setQcMark(auditIn.getQcMark());
                wmsInventoryMapper.insert(wmsInventory);
            }

            //状态修改
            WmsOrderLine wmsOrderLine = wmsOrderLineMapper.selectById(auditIn.getOrderLineId());
            WmsOrderHeader wmsOrderHeader = wmsOrderHeaderMapper.selectOne(new LambdaQueryWrapper<WmsOrderHeader>().eq(WmsOrderHeader::getOrderNum, wmsOrderLine.getOrderNum()));
            if (wmsAuditInvo.getShelfDjCount().equals(wmsAuditInvo.getAuditCount())) {
                //已上架状态
                wmsAuditInvo.setShelfStatus(CommonWms.END_FOR_SHELF);
                this.updateById(wmsAuditInvo);
                if (wmsOrderLine != null) {
                    List<WmsAuditIn> auditIns = this.baseMapper.selectList(Wrappers.<WmsAuditIn>lambdaQuery()
                            .eq(WmsAuditIn::getOrderLineId, auditIn.getOrderLineId()));
                    List<WmsAuditIn> auditInList = auditIns.stream().filter(line -> line.getShelfStatus().equals(CommonWms.END_FOR_SHELF)).collect(Collectors.toList());
                    if (auditIns.size() == auditInList.size()) {
                        //明细状态
                        wmsOrderLine.setLineStatus(CommonWms.END_FOR_SHELF);
                        wmsOrderLineMapper.updateById(wmsOrderLine);
                        if (wmsOrderHeader != null) {
                            //主单状态
                            List<WmsOrderLine> orderLines = wmsOrderLineMapper.selectList(Wrappers.<WmsOrderLine>lambdaQuery()
                                    .eq(WmsOrderLine::getOrderNum, wmsOrderLine.getOrderNum()));
                            List<WmsOrderLine> receivedList = orderLines.stream().filter(line -> line.getLineStatus().equals(CommonWms.END_FOR_SHELF)).collect(Collectors.toList());
                            if (orderLines.size() == receivedList.size()) {
                                wmsOrderHeader.setOrderStatus(CommonWms.END_FOR_SHELF);
                                wmsOrderHeaderMapper.updateById(wmsOrderHeader);
                            }
                        }
                    }
                }
            }else {
                wmsAuditInvo.setShelfStatus(CommonWms.BEGIN_FOR_SHELF);
                this.updateById(wmsAuditInvo);
                //修改明细状态
                wmsOrderLine.setLineStatus(CommonWms.BEGIN_FOR_SHELF);
                wmsOrderLineMapper.updateById(wmsOrderLine);
                //修改单据状态
                if (wmsOrderHeader != null) {
                    wmsOrderHeader.setOrderStatus(CommonWms.BEGIN_FOR_SHELF);
                    wmsOrderHeaderMapper.updateById(wmsOrderHeader);
                }
            }
        }
    }

}
