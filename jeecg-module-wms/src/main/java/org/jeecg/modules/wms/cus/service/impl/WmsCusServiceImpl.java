package org.jeecg.modules.wms.cus.service.impl;

import org.jeecg.modules.wms.cus.entity.WmsCus;
import org.jeecg.modules.wms.cus.mapper.WmsCusMapper;
import org.jeecg.modules.wms.cus.service.IWmsCusService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 客户
 * @Author: jeecg-boot
 * @Date:   2023-11-10
 * @Version: V1.0
 */
@Service
public class WmsCusServiceImpl extends ServiceImpl<WmsCusMapper, WmsCus> implements IWmsCusService {

}
