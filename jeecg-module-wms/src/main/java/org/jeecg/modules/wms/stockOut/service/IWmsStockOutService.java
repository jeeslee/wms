package org.jeecg.modules.wms.stockOut.service;

import org.jeecg.modules.wms.stockOut.entity.WmsStockOut;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 已下架表
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface IWmsStockOutService extends IService<WmsStockOut> {

}
