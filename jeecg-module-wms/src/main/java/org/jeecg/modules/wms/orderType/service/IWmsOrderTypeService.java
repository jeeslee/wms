package org.jeecg.modules.wms.orderType.service;

import org.jeecg.modules.wms.orderType.entity.WmsOrderType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 单据类型
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface IWmsOrderTypeService extends IService<WmsOrderType> {

}
