package org.jeecg.modules.wms.outOrder.vo;

import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import org.jeecg.modules.wms.outOrder.entity.WmsOutOrderHeader;
import org.jeecg.modules.wms.outOrder.entity.WmsOutOrderLine;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 出库单主表
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Data
@ApiModel(value="wms_out_order_headerPage对象", description="出库单主表")
public class WmsOutOrderHeaderPage {


	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
	@ApiModelProperty(value = "主键")
	private String id;
	/**创建人登录名称*/
	@ApiModelProperty(value = "创建人登录名称")
	private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "创建日期")
	private Date createTime;
	/**更新人登录名称*/
	@ApiModelProperty(value = "更新人登录名称")
	private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "更新日期")
	private Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private String sysOrgCode;
	/**所属公司*/
	@Excel(name = "所属公司", width = 15)
	@ApiModelProperty(value = "所属公司")
	private String sysCompanyCode;
	/**客户*/
	@Excel(name = "客户", width = 15)
	@ApiModelProperty(value = "客户")
	private String cusCode;
	/**要求交货时间*/
	@Excel(name = "要求交货时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "要求交货时间")
	private Date receiveDate;
	/**收货人*/
	@Excel(name = "收货人", width = 15)
	@ApiModelProperty(value = "收货人")
	private String receiverCode;
	/**发货月台*/
	@Excel(name = "发货月台", width = 15)
	@ApiModelProperty(value = "发货月台")
	private String platCode;
	/**状态*/
	@Excel(name = "状态", width = 15)
	@ApiModelProperty(value = "状态")
	private String orderStatus;
	/**出货单号*/
	@Excel(name = "出货单号", width = 15)
	@ApiModelProperty(value = "出货单号")
	private String orderNum;

	/**订单类型*/
	@Excel(name = "订单类型", width = 15)
	@ApiModelProperty(value = "订单类型")
	private String orderTypeCode;
	/**订单类型名称*/
	@Excel(name = "订单类型名称", width = 15)
	@ApiModelProperty(value = "订单类型名称")
	private String orderTypeName;

	/**客户名称*/
	@Excel(name = "客户名称", width = 15)
	@ApiModelProperty(value = "客户名称")
	private String cusName;
	/**租户ID*/
	@Excel(name = "租户ID", width = 15)
	@ApiModelProperty(value = "租户ID")
	private Integer tenantId;
	/**供应商编码*/
	@Excel(name = "供应商编码", width = 15)
	@ApiModelProperty(value = "供应商编码")
	private String supCode;
	/**供应商名称*/
	@Excel(name = "供应商名称", width = 15)
	@ApiModelProperty(value = "供应商名称")
	private String supName;
	/**备注*/
	@Excel(name = "备注", width = 15)
	@ApiModelProperty(value = "备注")
	private String remark;
	/**删除状态(0-正常,1-已删除)*/
	@Excel(name = "删除状态(0-正常,1-已删除)", width = 15)
	@ApiModelProperty(value = "删除状态(0-正常,1-已删除)")
	@TableLogic
	private Integer delFlag;
	@ExcelCollection(name="出库单子表")
	@ApiModelProperty(value = "出库单子表")
	private List<WmsOutOrderLine> wmsOutOrderLineList;
	
}
