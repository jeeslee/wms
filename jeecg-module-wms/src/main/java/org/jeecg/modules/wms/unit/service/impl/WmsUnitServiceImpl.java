package org.jeecg.modules.wms.unit.service.impl;

import org.jeecg.modules.wms.unit.entity.WmsUnit;
import org.jeecg.modules.wms.unit.mapper.WmsUnitMapper;
import org.jeecg.modules.wms.unit.service.IWmsUnitService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 单位
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Service
public class WmsUnitServiceImpl extends ServiceImpl<WmsUnitMapper, WmsUnit> implements IWmsUnitService {

}
