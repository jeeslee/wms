package org.jeecg.modules.wms.inOrder.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.wms.inOrder.entity.WmsOrderHeader;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 入库单主表
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface WmsOrderHeaderMapper extends BaseMapper<WmsOrderHeader> {

}
