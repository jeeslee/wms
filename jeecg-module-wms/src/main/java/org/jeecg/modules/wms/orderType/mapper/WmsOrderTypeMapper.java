package org.jeecg.modules.wms.orderType.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.wms.orderType.entity.WmsOrderType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 单据类型
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface WmsOrderTypeMapper extends BaseMapper<WmsOrderType> {

}
