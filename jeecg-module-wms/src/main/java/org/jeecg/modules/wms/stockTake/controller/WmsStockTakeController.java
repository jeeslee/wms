package org.jeecg.modules.wms.stockTake.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.wms.common.constant.CommonWms;
import org.jeecg.modules.wms.stockTake.entity.WmsStockTake;
import org.jeecg.modules.wms.stockTake.service.IWmsStockTakeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.jar.JarException;

/**
 * @Description: 盘点单
 * @Author: jeecg-boot
 * @Date:   2023-12-21
 * @Version: V1.0
 */
@Api(tags="盘点单")
@RestController
@RequestMapping("/stockTake/wmsStockTake")
@Slf4j
public class WmsStockTakeController extends JeecgController<WmsStockTake, IWmsStockTakeService> {
	@Autowired
	private IWmsStockTakeService wmsStockTakeService;
	
	/**
	 * 分页列表查询
	 *
	 * @param wmsStockTake
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "盘点单-分页列表查询")
	@ApiOperation(value="盘点单-分页列表查询", notes="盘点单-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<WmsStockTake>> queryPageList(WmsStockTake wmsStockTake,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<WmsStockTake> queryWrapper = QueryGenerator.initQueryWrapper(wmsStockTake, req.getParameterMap());
		queryWrapper.eq("take_status", CommonWms.TAKE_STATUS_DPD);
		Page<WmsStockTake> page = new Page<WmsStockTake>(pageNo, pageSize);
		IPage<WmsStockTake> pageList = wmsStockTakeService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param wmsStockTake
	 * @return
	 */
	@AutoLog(value = "盘点单-添加")
	@ApiOperation(value="盘点单-添加", notes="盘点单-添加")
	@RequiresPermissions("stockTake:wms_stock_take:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody WmsStockTake wmsStockTake) {
		wmsStockTakeService.save(wmsStockTake);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  盘点
	 *
	 * @param wmsStockTake
	 * @return
	 */
	@AutoLog(value = "盘点单-盘点")
	@ApiOperation(value="盘点单-盘点", notes="盘点单-盘点")
	@RequiresPermissions("stockTake:wms_stock_take:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody WmsStockTake wmsStockTake) {
		wmsStockTakeService.stockEdit(wmsStockTake);
		return Result.OK("盘点完成!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "盘点单-通过id删除")
	@ApiOperation(value="盘点单-通过id删除", notes="盘点单-通过id删除")
	@RequiresPermissions("stockTake:wms_stock_take:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		wmsStockTakeService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "盘点单-批量删除")
	@ApiOperation(value="盘点单-批量删除", notes="盘点单-批量删除")
	@RequiresPermissions("stockTake:wms_stock_take:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.wmsStockTakeService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}

	 /**
	  *  调账
	  *
	  * @param ids
	  * @return
	  */
	 @AutoLog(value = "盘点单-调账")
	 @ApiOperation(value="盘点单-调账", notes="盘点单-调账")
	 @RequiresPermissions("stockTake:wms_stock_take:deleteBatch")
	 @PostMapping(value = "/adjustAccount")
	 public Result<String> adjustAccount(@RequestParam(name="ids",required=true) String ids) {
	 	if (StrUtil.isEmpty(ids)){
	 		throw new JeecgBootException("参数不能为空");
		}
		 this.wmsStockTakeService.adjustAccount(Arrays.asList(ids.split(",")));
		 return Result.OK("调账成功!");
	 }
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "盘点单-通过id查询")
	@ApiOperation(value="盘点单-通过id查询", notes="盘点单-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<WmsStockTake> queryById(@RequestParam(name="id",required=true) String id) {
		WmsStockTake wmsStockTake = wmsStockTakeService.getById(id);
		if(wmsStockTake==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(wmsStockTake);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param wmsStockTake
    */
    @RequiresPermissions("stockTake:wms_stock_take:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, WmsStockTake wmsStockTake) {
        return super.exportXls(request, wmsStockTake, WmsStockTake.class, "盘点单");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequiresPermissions("stockTake:wms_stock_take:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, WmsStockTake.class);
    }
}