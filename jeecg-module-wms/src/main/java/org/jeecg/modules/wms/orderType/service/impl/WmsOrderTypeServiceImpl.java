package org.jeecg.modules.wms.orderType.service.impl;

import org.jeecg.modules.wms.orderType.entity.WmsOrderType;
import org.jeecg.modules.wms.orderType.mapper.WmsOrderTypeMapper;
import org.jeecg.modules.wms.orderType.service.IWmsOrderTypeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 单据类型
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Service
public class WmsOrderTypeServiceImpl extends ServiceImpl<WmsOrderTypeMapper, WmsOrderType> implements IWmsOrderTypeService {

}
