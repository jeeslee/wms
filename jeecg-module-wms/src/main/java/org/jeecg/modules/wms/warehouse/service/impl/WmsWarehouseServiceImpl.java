package org.jeecg.modules.wms.warehouse.service.impl;

import org.jeecg.modules.wms.warehouse.entity.WmsWarehouse;
import org.jeecg.modules.wms.warehouse.mapper.WmsWarehouseMapper;
import org.jeecg.modules.wms.warehouse.service.IWmsWarehouseService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 仓库
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Service
public class WmsWarehouseServiceImpl extends ServiceImpl<WmsWarehouseMapper, WmsWarehouse> implements IWmsWarehouseService {

}
