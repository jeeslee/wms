package org.jeecg.modules.wms.stockInChange.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.wms.stockInChange.entity.WmsStockInChange;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 上架调整记录表
 * @Author: jeecg-boot
 * @Date:   2023-11-28
 * @Version: V1.0
 */
public interface WmsStockInChangeMapper extends BaseMapper<WmsStockInChange> {

}
