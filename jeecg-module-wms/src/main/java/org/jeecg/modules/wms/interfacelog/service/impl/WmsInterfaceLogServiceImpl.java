package org.jeecg.modules.wms.interfacelog.service.impl;

import org.jeecg.modules.wms.interfacelog.entity.WmsInterfaceLog;
import org.jeecg.modules.wms.interfacelog.mapper.WmsInterfaceLogMapper;
import org.jeecg.modules.wms.interfacelog.service.IWmsInterfaceLogService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 接口日志表
 * @Author: jeecg-boot
 * @Date:   2024-01-10
 * @Version: V1.0
 */
@Service
public class WmsInterfaceLogServiceImpl extends ServiceImpl<WmsInterfaceLogMapper, WmsInterfaceLog> implements IWmsInterfaceLogService {

}
