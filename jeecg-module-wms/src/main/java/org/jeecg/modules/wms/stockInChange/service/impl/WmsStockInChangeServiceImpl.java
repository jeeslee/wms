package org.jeecg.modules.wms.stockInChange.service.impl;

import org.jeecg.modules.wms.stockInChange.entity.WmsStockInChange;
import org.jeecg.modules.wms.stockInChange.mapper.WmsStockInChangeMapper;
import org.jeecg.modules.wms.stockInChange.service.IWmsStockInChangeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 上架调整记录表
 * @Author: jeecg-boot
 * @Date:   2023-11-28
 * @Version: V1.0
 */
@Service
public class WmsStockInChangeServiceImpl extends ServiceImpl<WmsStockInChangeMapper, WmsStockInChange> implements IWmsStockInChangeService {

}
