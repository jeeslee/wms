package org.jeecg.modules.wms.supply.service.impl;

import org.jeecg.modules.wms.supply.entity.WmsSupply;
import org.jeecg.modules.wms.supply.mapper.WmsSupplyMapper;
import org.jeecg.modules.wms.supply.service.IWmsSupplyService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 供应商
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Service
public class WmsSupplyServiceImpl extends ServiceImpl<WmsSupplyMapper, WmsSupply> implements IWmsSupplyService {

}
