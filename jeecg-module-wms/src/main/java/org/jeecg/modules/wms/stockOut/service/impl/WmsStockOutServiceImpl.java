package org.jeecg.modules.wms.stockOut.service.impl;

import org.jeecg.modules.wms.stockOut.entity.WmsStockOut;
import org.jeecg.modules.wms.stockOut.mapper.WmsStockOutMapper;
import org.jeecg.modules.wms.stockOut.service.IWmsStockOutService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 已下架表
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Service
public class WmsStockOutServiceImpl extends ServiceImpl<WmsStockOutMapper, WmsStockOut> implements IWmsStockOutService {

}
