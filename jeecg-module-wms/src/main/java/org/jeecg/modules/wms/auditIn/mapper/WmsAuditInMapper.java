package org.jeecg.modules.wms.auditIn.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.wms.auditIn.entity.WmsAuditIn;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 待上架表
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface WmsAuditInMapper extends BaseMapper<WmsAuditIn> {

}
