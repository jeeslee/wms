package org.jeecg.modules.wms.stockIn.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.wms.stockIn.entity.WmsStockIn;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 已上架表
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface WmsStockInMapper extends BaseMapper<WmsStockIn> {

}
