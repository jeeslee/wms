package org.jeecg.modules.wms.stockIn.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.wms.stockIn.entity.WmsStockIn;
import org.jeecg.modules.wms.stockIn.service.IWmsStockInService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.authz.annotation.RequiresPermissions;

 /**
 * @Description: 已上架表
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Api(tags="已上架表")
@RestController
@RequestMapping("/stockIn/wmsStockIn")
@Slf4j
public class WmsStockInController extends JeecgController<WmsStockIn, IWmsStockInService> {
	@Autowired
	private IWmsStockInService wmsStockInService;
	
	/**
	 * 分页列表查询
	 *
	 * @param wmsStockIn
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "已上架表-分页列表查询")
	@ApiOperation(value="已上架表-分页列表查询", notes="已上架表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<WmsStockIn>> queryPageList(WmsStockIn wmsStockIn,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<WmsStockIn> queryWrapper = QueryGenerator.initQueryWrapper(wmsStockIn, req.getParameterMap());
		Page<WmsStockIn> page = new Page<WmsStockIn>(pageNo, pageSize);
		IPage<WmsStockIn> pageList = wmsStockInService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param wmsStockIn
	 * @return
	 */
	@AutoLog(value = "已上架表-添加")
	@ApiOperation(value="已上架表-添加", notes="已上架表-添加")
	@RequiresPermissions("stockIn:wms_stock_in:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody WmsStockIn wmsStockIn) {
		wmsStockInService.save(wmsStockIn);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param wmsStockIn
	 * @return
	 */
	@AutoLog(value = "已上架表-编辑")
	@ApiOperation(value="已上架表-编辑", notes="已上架表-编辑")
	@RequiresPermissions("stockIn:wms_stock_in:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody WmsStockIn wmsStockIn) {
		wmsStockInService.updateById(wmsStockIn);
		return Result.OK("编辑成功!");
	}

	 /**
	  *  上架调整
	  *
	  * @param wmsStockIn
	  * @return
	  */
	 @AutoLog(value = "已上架表-上架调整")
	 @ApiOperation(value="已上架表-上架调整", notes="已上架表-上架调整")
	 @RequiresPermissions("stockIn:wms_stock_in:edit")
	 @RequestMapping(value = "/onShelfChange", method = {RequestMethod.PUT,RequestMethod.POST})
	 public Result<String> onShelfChange(@RequestBody WmsStockIn wmsStockIn) {
		 wmsStockInService.onShelfChange(wmsStockIn);
		 return Result.OK("编辑成功!");
	 }
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "已上架表-通过id删除")
	@ApiOperation(value="已上架表-通过id删除", notes="已上架表-通过id删除")
	@RequiresPermissions("stockIn:wms_stock_in:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		wmsStockInService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "已上架表-批量删除")
	@ApiOperation(value="已上架表-批量删除", notes="已上架表-批量删除")
	@RequiresPermissions("stockIn:wms_stock_in:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.wmsStockInService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "已上架表-通过id查询")
	@ApiOperation(value="已上架表-通过id查询", notes="已上架表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<WmsStockIn> queryById(@RequestParam(name="id",required=true) String id) {
		WmsStockIn wmsStockIn = wmsStockInService.getById(id);
		if(wmsStockIn==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(wmsStockIn);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param wmsStockIn
    */
    @RequiresPermissions("stockIn:wms_stock_in:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, WmsStockIn wmsStockIn) {
        return super.exportXls(request, wmsStockIn, WmsStockIn.class, "已上架表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequiresPermissions("stockIn:wms_stock_in:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, WmsStockIn.class);
    }

}
