package org.jeecg.modules.wms.inventory.service;

import org.jeecg.modules.wms.inventory.entity.WmsInventory;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 即时库存
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface IWmsInventoryService extends IService<WmsInventory> {

    void labelAllocation(WmsInventory wmsInventory);

    void kwAllocation(WmsInventory wmsInventory);

    void trayAllocation(WmsInventory wmsInventory);

    void openlyTake(List<String> asList);

    void clouseTake(List<String> asList);
}
