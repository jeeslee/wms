package org.jeecg.modules.wms.plat.service.impl;

import org.jeecg.modules.wms.plat.entity.WmsPlat;
import org.jeecg.modules.wms.plat.mapper.WmsPlatMapper;
import org.jeecg.modules.wms.plat.service.IWmsPlatService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 月台
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Service
public class WmsPlatServiceImpl extends ServiceImpl<WmsPlatMapper, WmsPlat> implements IWmsPlatService {

}
