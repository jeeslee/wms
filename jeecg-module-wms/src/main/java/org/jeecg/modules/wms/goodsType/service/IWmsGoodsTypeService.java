package org.jeecg.modules.wms.goodsType.service;

import org.jeecg.modules.wms.goodsType.entity.WmsGoodsType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 物料类型
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface IWmsGoodsTypeService extends IService<WmsGoodsType> {

}
