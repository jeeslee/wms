package org.jeecg.modules.wms.inOrder.service.impl;

import cn.hutool.core.date.DateUtil;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.util.DateUtils;
import org.jeecg.modules.wms.common.constant.CommonWms;
import org.jeecg.modules.wms.common.service.IWmsGetCommonServiceImpl;
import org.jeecg.modules.wms.inOrder.entity.WmsOrderHeader;
import org.jeecg.modules.wms.inOrder.entity.WmsOrderLine;
import org.jeecg.modules.wms.inOrder.mapper.WmsOrderLineMapper;
import org.jeecg.modules.wms.inOrder.mapper.WmsOrderHeaderMapper;
import org.jeecg.modules.wms.inOrder.service.IWmsOrderHeaderService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 入库单主表
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Service
public class WmsOrderHeaderServiceImpl extends ServiceImpl<WmsOrderHeaderMapper, WmsOrderHeader> implements IWmsOrderHeaderService {

	@Autowired
	private WmsOrderHeaderMapper wmsOrderHeaderMapper;
	@Autowired
	private WmsOrderLineMapper wmsOrderLineMapper;
	@Autowired
	private IWmsGetCommonServiceImpl wmsGetNameService;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveMain(WmsOrderHeader wmsOrderHeader, List<WmsOrderLine> wmsOrderLineList) {
		wmsOrderHeaderMapper.insert(wmsOrderHeader);
		if(wmsOrderLineList!=null && wmsOrderLineList.size()>0) {
			for(WmsOrderLine entity:wmsOrderLineList) {
				//外键设置
				entity.setOrderNum(wmsOrderHeader.getOrderNum());
				wmsOrderLineMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateMain(WmsOrderHeader wmsOrderHeader,List<WmsOrderLine> wmsOrderLineList) {
		//组装欠缺名称
//		wmsOrderHeader.setCusName(wmsGetNameService.getName(wmsOrderHeader.getCusCode(), CommonWms.CUS_CODE));
//		wmsOrderHeader.setSupName(wmsGetNameService.getName(wmsOrderHeader.getSupCode(), CommonWms.SUPLY_CODE));
//		wmsOrderHeader.setOrderTypeName(wmsGetNameService.getName(wmsOrderHeader.getOrderTypeCode(), CommonWms.ORDER_TYPE_CODE));

		wmsOrderHeaderMapper.updateById(wmsOrderHeader);

		//1.先删除子表数据
		wmsOrderLineMapper.deleteByMainId(wmsOrderHeader.getOrderNum());
		
		//2.子表数据重新插入
		if(wmsOrderLineList!=null && wmsOrderLineList.size()>0) {
			for(WmsOrderLine entity:wmsOrderLineList) {
				//外键设置
				entity.setLineStatus(CommonWms.WAIT_FOR_CHECK);
				entity.setOrderNum(wmsOrderHeader.getOrderNum());
				entity.setGoodsSyCount(entity.getGoodsCount());
				entity.setGoodsDjCount("0");
				entity.setGoodsUnitName(wmsGetNameService.getName(entity.getGoodsUnitCode(), CommonWms.UNIT_CODE));
				wmsOrderLineMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void delMain(String id) {
		WmsOrderHeader wmsOrderHeader = wmsOrderHeaderMapper.selectById(id);
		wmsOrderLineMapper.deleteByMainId(wmsOrderHeader.getOrderNum());
		wmsOrderHeaderMapper.deleteById(id);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			WmsOrderHeader wmsOrderHeader = wmsOrderHeaderMapper.selectById(id);
			if (!wmsOrderHeader.getOrderStatus().equals(CommonWms.WAIT_FOR_CHECK)){
				throw new JeecgBootException("单据状态是"+wmsOrderHeader.getOrderStatus()+"，不可以删除");
			}
			wmsOrderLineMapper.deleteByMainId(wmsOrderHeader.getOrderNum());
			wmsOrderHeaderMapper.deleteById(id);
		}
	}

	@Override
	public void saveWmsOrderHeader(WmsOrderHeader wmsOrderHeader, List<WmsOrderLine> wmsOrderLineList) {
		setWmsOrderHeaderField(wmsOrderHeader);
		wmsOrderHeaderMapper.insert(wmsOrderHeader);
		if(wmsOrderLineList!=null && wmsOrderLineList.size()>0) {
			for(WmsOrderLine entity:wmsOrderLineList) {
				//外键设置
				entity.setOrderNum(wmsOrderHeader.getOrderNum());
				entity.setGoodsSyCount(entity.getGoodsCount());
				entity.setGoodsDjCount("0");
				entity.setCusCode(wmsOrderHeader.getCusCode());
				entity.setLineStatus(CommonWms.WAIT_FOR_CHECK);
				entity.setDelFlag(0);
				entity.setGoodsUnitName(wmsGetNameService.getName(entity.getGoodsUnitCode(), CommonWms.UNIT_CODE));
				wmsOrderLineMapper.insert(entity);
			}
		}
	}

	private void setWmsOrderHeaderField(WmsOrderHeader wmsOrderHeader) {
		//生成订单号
		int count = Math.toIntExact(this.lambdaQuery()
				.eq(WmsOrderHeader::getOrderTypeCode, wmsOrderHeader.getOrderTypeCode())
				.apply("date_format(create_time,'%Y-%m-%d') = '" + DateUtil.today() + "'")
				.count());
		wmsOrderHeader.setOrderNum(wmsOrderHeader.getOrderTypeCode()
				+ DateUtils.date2Str(new SimpleDateFormat("yyyyMMdd"))
				+ "-"
				+ String.format("%04d", (count + 1)));
		wmsOrderHeader.setOrderStatus(CommonWms.WAIT_FOR_CHECK);
		//组装欠缺名称
//		wmsOrderHeader.setCusName(wmsGetNameService.getName(wmsOrderHeader.getCusCode(), CommonWms.CUS_CODE));
//		wmsOrderHeader.setSupName(wmsGetNameService.getName(wmsOrderHeader.getSupCode(), CommonWms.SUPLY_CODE));
//		wmsOrderHeader.setOrderTypeName(wmsGetNameService.getName(wmsOrderHeader.getOrderTypeCode(), CommonWms.ORDER_TYPE_CODE));
	}

}
