package org.jeecg.modules.wms.plat.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.wms.plat.entity.WmsPlat;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 月台
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface WmsPlatMapper extends BaseMapper<WmsPlat> {

}
