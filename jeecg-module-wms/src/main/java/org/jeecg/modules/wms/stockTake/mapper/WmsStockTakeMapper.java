package org.jeecg.modules.wms.stockTake.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.wms.stockTake.entity.WmsStockTake;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 盘点单
 * @Author: jeecg-boot
 * @Date:   2023-12-21
 * @Version: V1.0
 */
public interface WmsStockTakeMapper extends BaseMapper<WmsStockTake> {

}
