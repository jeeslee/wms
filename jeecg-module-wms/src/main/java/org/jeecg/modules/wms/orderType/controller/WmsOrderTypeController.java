package org.jeecg.modules.wms.orderType.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.wms.orderType.entity.WmsOrderType;
import org.jeecg.modules.wms.orderType.service.IWmsOrderTypeService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.authz.annotation.RequiresPermissions;

 /**
 * @Description: 单据类型
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Api(tags="单据类型")
@RestController
@RequestMapping("/orderType/wmsOrderType")
@Slf4j
public class WmsOrderTypeController extends JeecgController<WmsOrderType, IWmsOrderTypeService> {
	@Autowired
	private IWmsOrderTypeService wmsOrderTypeService;
	
	/**
	 * 分页列表查询
	 *
	 * @param wmsOrderType
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "单据类型-分页列表查询")
	@ApiOperation(value="单据类型-分页列表查询", notes="单据类型-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<WmsOrderType>> queryPageList(WmsOrderType wmsOrderType,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<WmsOrderType> queryWrapper = QueryGenerator.initQueryWrapper(wmsOrderType, req.getParameterMap());
		Page<WmsOrderType> page = new Page<WmsOrderType>(pageNo, pageSize);
		IPage<WmsOrderType> pageList = wmsOrderTypeService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param wmsOrderType
	 * @return
	 */
	@AutoLog(value = "单据类型-添加")
	@ApiOperation(value="单据类型-添加", notes="单据类型-添加")
	@RequiresPermissions("orderType:wms_order_type:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody WmsOrderType wmsOrderType) {
		wmsOrderTypeService.save(wmsOrderType);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param wmsOrderType
	 * @return
	 */
	@AutoLog(value = "单据类型-编辑")
	@ApiOperation(value="单据类型-编辑", notes="单据类型-编辑")
	@RequiresPermissions("orderType:wms_order_type:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody WmsOrderType wmsOrderType) {
		wmsOrderTypeService.updateById(wmsOrderType);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "单据类型-通过id删除")
	@ApiOperation(value="单据类型-通过id删除", notes="单据类型-通过id删除")
	@RequiresPermissions("orderType:wms_order_type:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		wmsOrderTypeService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "单据类型-批量删除")
	@ApiOperation(value="单据类型-批量删除", notes="单据类型-批量删除")
	@RequiresPermissions("orderType:wms_order_type:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.wmsOrderTypeService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "单据类型-通过id查询")
	@ApiOperation(value="单据类型-通过id查询", notes="单据类型-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<WmsOrderType> queryById(@RequestParam(name="id",required=true) String id) {
		WmsOrderType wmsOrderType = wmsOrderTypeService.getById(id);
		if(wmsOrderType==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(wmsOrderType);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param wmsOrderType
    */
    @RequiresPermissions("orderType:wms_order_type:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, WmsOrderType wmsOrderType) {
        return super.exportXls(request, wmsOrderType, WmsOrderType.class, "单据类型");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequiresPermissions("orderType:wms_order_type:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, WmsOrderType.class);
    }

}
