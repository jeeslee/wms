package org.jeecg.modules.wms.inventory.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.wms.inventory.entity.WmsInventory;
import org.jeecg.modules.wms.inventory.service.IWmsInventoryService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.authz.annotation.RequiresPermissions;

 /**
 * @Description: 即时库存
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Api(tags="即时库存")
@RestController
@RequestMapping("/inventory/wmsInventory")
@Slf4j
public class WmsInventoryController extends JeecgController<WmsInventory, IWmsInventoryService> {
	@Autowired
	private IWmsInventoryService wmsInventoryService;
	
	/**
	 * 分页列表查询
	 *
	 * @param wmsInventory
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "即时库存-分页列表查询")
	@ApiOperation(value="即时库存-分页列表查询", notes="即时库存-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<WmsInventory>> queryPageList(WmsInventory wmsInventory,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<WmsInventory> queryWrapper = QueryGenerator.initQueryWrapper(wmsInventory, req.getParameterMap());
		queryWrapper.ne("base_count","0");
		Page<WmsInventory> page = new Page<WmsInventory>(pageNo, pageSize);
		IPage<WmsInventory> pageList = wmsInventoryService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param wmsInventory
	 * @return
	 */
	@AutoLog(value = "即时库存-添加")
	@ApiOperation(value="即时库存-添加", notes="即时库存-添加")
	@RequiresPermissions("inventory:wms_inventory:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody WmsInventory wmsInventory) {
		wmsInventoryService.save(wmsInventory);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param wmsInventory
	 * @return
	 */
	@AutoLog(value = "即时库存-编辑")
	@ApiOperation(value="即时库存-编辑", notes="即时库存-编辑")
	@RequiresPermissions("inventory:wms_inventory:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody WmsInventory wmsInventory) {
		wmsInventoryService.updateById(wmsInventory);
		return Result.OK("编辑成功!");
	}

	 /**
	  *  标签分配
	  *
	  * @param wmsInventory
	  * @return
	  */
	 @AutoLog(value = "即时库存-标签分配")
	 @ApiOperation(value="即时库存-标签分配", notes="即时库存-标签分配")
	 @RequiresPermissions("inventory:wms_inventory:edit")
	 @RequestMapping(value = "/labelAllocation", method = {RequestMethod.PUT,RequestMethod.POST})
	 public Result<String> labelAllocation(@RequestBody WmsInventory wmsInventory) {
	 	if (wmsInventory.getBinCode().equals(wmsInventory.getBinCodeNew())){
	 		throw new JeecgBootException("新标签不能和旧标签一致，请核对！");
		}
		 if (NumberUtil.compare(Convert.toDouble(wmsInventory.getBaseCount()),Convert.toDouble(wmsInventory.getBaseCountNew())) < 0){
			 throw new JeecgBootException("新标签数量不能大于旧标签数量，请核对！");
		 }
		 wmsInventoryService.labelAllocation(wmsInventory);
		 return Result.OK("标签分配成功!");
	 }

	 /**
	  *  库位分配
	  *
	  * @param wmsInventory
	  * @return
	  */
	 @AutoLog(value = "即时库存-库位分配")
	 @ApiOperation(value="即时库存-库位分配", notes="即时库存-库位分配")
	 @RequiresPermissions("inventory:wms_inventory:edit")
	 @RequestMapping(value = "/kwAllocation", method = {RequestMethod.PUT,RequestMethod.POST})
	 public Result<String> kwAllocation(@RequestBody WmsInventory wmsInventory) {
		 if (wmsInventory.getKwCode().equals(wmsInventory.getKwCodeNew())){
			 throw new JeecgBootException("新库位不能和旧库位一致，请核对！");
		 }
		 if (NumberUtil.compare(Convert.toDouble(wmsInventory.getBaseCount()),Convert.toDouble(wmsInventory.getBaseCountNew())) < 0){
			 throw new JeecgBootException("新库位数量不能大于旧库位数量，请核对！");
		 }
		 wmsInventoryService.kwAllocation(wmsInventory);
		 return Result.OK("库位分配成功!");
	 }

	 /**
	  *  托盘分配
	  *
	  * @param wmsInventory
	  * @return
	  */
	 @AutoLog(value = "即时库存-托盘分配")
	 @ApiOperation(value="即时库存-托盘分配", notes="即时库存-托盘分配")
	 @RequiresPermissions("inventory:wms_inventory:edit")
	 @RequestMapping(value = "/trayAllocation", method = {RequestMethod.PUT,RequestMethod.POST})
	 public Result<String> trayAllocation(@RequestBody WmsInventory wmsInventory) {
		 if (wmsInventory.getTrayCode().equals(wmsInventory.getTrayCodeNew())){
			 throw new JeecgBootException("新托盘不能和旧托盘一致，请核对！");
		 }
		 if (NumberUtil.compare(Convert.toDouble(wmsInventory.getBaseCount()),Convert.toDouble(wmsInventory.getBaseCountNew())) < 0){
			 throw new JeecgBootException("新托盘数量不能大于旧托盘数量，请核对！");
		 }
		 wmsInventoryService.trayAllocation(wmsInventory);
		 return Result.OK("库位分配成功!");
	 }
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "即时库存-通过id删除")
	@ApiOperation(value="即时库存-通过id删除", notes="即时库存-通过id删除")
	@RequiresPermissions("inventory:wms_inventory:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		wmsInventoryService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "即时库存-批量删除")
	@ApiOperation(value="即时库存-批量删除", notes="即时库存-批量删除")
	@RequiresPermissions("inventory:wms_inventory:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.wmsInventoryService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}


	 /**
	  *  库存盘点：明盘
	  *
	  * @param ids
	  * @return
	  */
	 @AutoLog(value = "即时库存-明盘")
	 @ApiOperation(value="即时库存-明盘", notes="即时库存-明盘")
	 @RequiresPermissions("inventory:wms_inventory:deleteBatch")
	 @PostMapping(value = "/openlyTake")
	 public Result<String> openlyTake(@RequestParam(name="ids",required=true) String ids) {
	 	if (StrUtil.isEmpty(ids)){
	 		throw new JeecgBootException("参数不能为空");
		}
		 this.wmsInventoryService.openlyTake(Arrays.asList(ids.split(",")));
		 return Result.OK("批量明盘成功!");
	 }

	 /**
	  *  库存盘点：暗盘
	  *
	  * @param ids
	  * @return clouseTake
	  */
	 @AutoLog(value = "即时库存-暗盘")
	 @ApiOperation(value="即时库存-暗盘", notes="即时库存-暗盘")
	 @RequiresPermissions("inventory:wms_inventory:deleteBatch")
	 @PostMapping(value = "/clouseTake")
	 public Result<String> clouseTake(@RequestParam(name="ids",required=true) String ids) {
		 if (StrUtil.isEmpty(ids)){
			 throw new JeecgBootException("参数不能为空");
		 }
		 this.wmsInventoryService.clouseTake(Arrays.asList(ids.split(",")));
		 return Result.OK("批量暗盘成功!");
	 }
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "即时库存-通过id查询")
	@ApiOperation(value="即时库存-通过id查询", notes="即时库存-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<WmsInventory> queryById(@RequestParam(name="id",required=true) String id) {
		WmsInventory wmsInventory = wmsInventoryService.getById(id);
		if(wmsInventory==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(wmsInventory);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param wmsInventory
    */
    @RequiresPermissions("inventory:wms_inventory:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, WmsInventory wmsInventory) {
        return super.exportXls(request, wmsInventory, WmsInventory.class, "即时库存");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequiresPermissions("inventory:wms_inventory:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, WmsInventory.class);
    }

}
