package org.jeecg.modules.wms.plat.service;

import org.jeecg.modules.wms.plat.entity.WmsPlat;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 月台
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface IWmsPlatService extends IService<WmsPlat> {

}
