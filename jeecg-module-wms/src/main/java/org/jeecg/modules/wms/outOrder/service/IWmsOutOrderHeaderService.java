package org.jeecg.modules.wms.outOrder.service;

import org.jeecg.modules.wms.outOrder.entity.WmsOutOrderLine;
import org.jeecg.modules.wms.outOrder.entity.WmsOutOrderHeader;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 出库单主表
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface IWmsOutOrderHeaderService extends IService<WmsOutOrderHeader> {

	/**
	 * 添加一对多
	 *
	 * @param wmsOutOrderHeader
	 * @param wmsOutOrderLineList
	 */
	public void saveMain(WmsOutOrderHeader wmsOutOrderHeader, List<WmsOutOrderLine> wmsOutOrderLineList) ;
	
	/**
	 * 修改一对多
	 *
	 * @param wmsOutOrderHeader
	 * @param wmsOutOrderLineList
	 */
	public void updateMain(WmsOutOrderHeader wmsOutOrderHeader, List<WmsOutOrderLine> wmsOutOrderLineList);
	
	/**
	 * 删除一对多
	 *
	 * @param id
	 */
	public void delMain(String id);
	
	/**
	 * 批量删除一对多
	 *
	 * @param idList
	 */
	public void delBatchMain(Collection<? extends Serializable> idList);

	/**
	 * 添加一对多
	 *
	 * @param wmsOutOrderHeader
	 * @param wmsOutOrderLineList
	 */
    void saveMainOutOrderHeader(WmsOutOrderHeader wmsOutOrderHeader, List<WmsOutOrderLine> wmsOutOrderLineList);
}
