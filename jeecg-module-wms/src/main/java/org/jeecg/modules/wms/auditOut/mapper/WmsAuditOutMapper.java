package org.jeecg.modules.wms.auditOut.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.wms.auditOut.entity.WmsAuditOut;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 待下架表
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface WmsAuditOutMapper extends BaseMapper<WmsAuditOut> {

}
