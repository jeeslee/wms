package org.jeecg.modules.wms.warehouseArea.service;

import org.jeecg.modules.wms.warehouseArea.entity.WmsWarehouseArea;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 库区
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface IWmsWarehouseAreaService extends IService<WmsWarehouseArea> {

}
