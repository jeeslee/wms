package org.jeecg.modules.wms.goods.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.wms.goods.entity.WmsGoods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 物料
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface WmsGoodsMapper extends BaseMapper<WmsGoods> {

}
