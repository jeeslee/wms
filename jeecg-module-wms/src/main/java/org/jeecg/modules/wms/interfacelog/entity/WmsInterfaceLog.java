package org.jeecg.modules.wms.interfacelog.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 接口日志表
 * @Author: jeecg-boot
 * @Date:   2024-01-10
 * @Version: V1.0
 */
@Data
@TableName("wms_interface_log")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="wms_interface_log对象", description="接口日志表")
public class WmsInterfaceLog implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**日志类型*/
	@Excel(name = "日志类型", width = 15)
    @ApiModelProperty(value = "日志类型")
    private String logType;
	/**日志标题*/
	@Excel(name = "日志标题", width = 15)
    @ApiModelProperty(value = "日志标题")
    private String logTitle;
	/**请求URL*/
	@Excel(name = "请求URL", width = 15)
    @ApiModelProperty(value = "请求URL")
    private String requestUrl;
	/**请求方法*/
	@Excel(name = "请求方法", width = 15)
    @ApiModelProperty(value = "请求方法")
    private String requestMethod;
	/**请求参数*/
	@Excel(name = "请求参数", width = 15)
    @ApiModelProperty(value = "请求参数")
    private String requestParams;
	/**执行时间*/
	@Excel(name = "执行时间", width = 15)
    @ApiModelProperty(value = "执行时间")
    private String requestTime;
	/**响应内容*/
	@Excel(name = "响应内容", width = 15)
    @ApiModelProperty(value = "响应内容")
    private String responseContent;
}
