package org.jeecg.modules.wms.auditOut.service.impl;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.NumberUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.modules.wms.auditOut.entity.WmsAuditOut;
import org.jeecg.modules.wms.auditOut.mapper.WmsAuditOutMapper;
import org.jeecg.modules.wms.auditOut.service.IWmsAuditOutService;
import org.jeecg.modules.wms.common.constant.CommonWms;
import org.jeecg.modules.wms.inventory.entity.WmsInventory;
import org.jeecg.modules.wms.inventory.mapper.WmsInventoryMapper;
import org.jeecg.modules.wms.outOrder.entity.WmsOutOrderHeader;
import org.jeecg.modules.wms.outOrder.entity.WmsOutOrderLine;
import org.jeecg.modules.wms.outOrder.mapper.WmsOutOrderHeaderMapper;
import org.jeecg.modules.wms.outOrder.mapper.WmsOutOrderLineMapper;
import org.jeecg.modules.wms.stockOut.entity.WmsStockOut;
import org.jeecg.modules.wms.stockOut.mapper.WmsStockOutMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description: 待下架表
 * @Author: jeecg-boot
 * @Date: 2023-11-13
 * @Version: V1.0
 */
@Service
public class WmsAuditOutServiceImpl extends ServiceImpl<WmsAuditOutMapper, WmsAuditOut> implements IWmsAuditOutService {

    @Autowired
    private WmsOutOrderLineMapper wmsOutOrderLineMapper;

    @Autowired
    private WmsOutOrderHeaderMapper wmsOutOrderHeaderMapper;

    @Autowired
    private WmsStockOutMapper wmsStockOutMapper;

    @Autowired
    private WmsInventoryMapper wmsInventoryMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void shelfOff(WmsAuditOut wmsAuditOut) {
        WmsAuditOut wmsAuditOutOld = this.baseMapper.selectById(wmsAuditOut.getId());
        //本次下架数量
        String shelfOffQua = wmsAuditOut.getShelfOffQua();
        //需求数量
        String goodsQua = wmsAuditOut.getGoodsQua();

        if (new BigDecimal(shelfOffQua).compareTo(new BigDecimal(wmsAuditOutOld.getShelfSyQua())) > 0) {
            throw new JeecgBootException("本次下架数量大于需求数量，请重新输入！");
        }

        WmsOutOrderHeader outOrderHeader = wmsOutOrderHeaderMapper.selectOne(new LambdaQueryWrapper<WmsOutOrderHeader>()
                .eq(WmsOutOrderHeader::getOrderNum, wmsAuditOut.getOrderNum())
        );
        //仓库记录  getBinCode   getKwCode  getTrayCode  getGoodsCode
        LambdaQueryWrapper<WmsInventory> lambdaQueryWrapper = new LambdaQueryWrapper<WmsInventory>()
                .eq(WmsInventory::getBinCode, wmsAuditOut.getBinCode())
                .eq(WmsInventory::getKwCode, wmsAuditOut.getKwCode())
                .eq(WmsInventory::getTrayCode, wmsAuditOut.getTrayCode())
                .eq(WmsInventory::getCusCode, wmsAuditOutOld.getCusCode())
                .eq(WmsInventory::getGoodsBatch, wmsAuditOutOld.getGoodsBatch())
//                .eq(WmsInventory::getGoodsProDate, wmsAuditOutOld.getGoodsProDate())
                .eq(WmsInventory::getGoodsUnitCode, wmsAuditOutOld.getGoodsUnitCode())
                .eq(WmsInventory::getGoodsCode, wmsAuditOutOld.getGoodsCode());
        if (outOrderHeader.getOrderTypeCode().equals("BLPCK")){
            lambdaQueryWrapper.eq(WmsInventory::getQcMark, CommonWms.QC_BAD_MARK);
        }else {
            lambdaQueryWrapper.eq(WmsInventory::getQcMark, CommonWms.QC_GOOD_MARK);
        }
        WmsInventory wmsInventories = wmsInventoryMapper.selectOne(lambdaQueryWrapper);

        if (wmsInventories == null) {
            throw new JeecgBootException("仓库无数据：物料编码 "+wmsAuditOutOld.getGoodsCode()+"物料名称 "+wmsAuditOutOld.getGoodsName());
        }

        if (NumberUtil.compare(Convert.toDouble(wmsInventories.getBaseCount()),Convert.toDouble(shelfOffQua)) >= 0){
            wmsAuditOutOld.setShelfOkQua(Convert.toStr(NumberUtil.add(shelfOffQua, wmsAuditOutOld.getShelfOkQua())));
            wmsAuditOutOld.setShelfSyQua(Convert.toStr(NumberUtil.sub(goodsQua, wmsAuditOutOld.getShelfOkQua())));

            //库存够扣情况
            BigDecimal subs = NumberUtil.sub(wmsInventories.getBaseCount(), Convert.toStr(shelfOffQua));
            wmsInventories.setBaseCount(Convert.toStr(subs));
            wmsInventoryMapper.updateById(wmsInventories);

            //生成下架信息
            WmsStockOut wmsStockOut = new WmsStockOut();
            wmsStockOut.setGoodsCode(wmsAuditOutOld.getGoodsCode());
            wmsStockOut.setGoodsQua(wmsAuditOut.getShelfOffQua());
            wmsStockOut.setAuditId(wmsAuditOutOld.getId());
            wmsStockOut.setGoodsUnitCode(wmsAuditOutOld.getGoodsUnitCode());
            wmsStockOut.setGoodsUnitName(wmsAuditOutOld.getGoodsUnitName());
            wmsStockOut.setGoodsProDate(wmsAuditOutOld.getGoodsProDate());
            wmsStockOut.setGoodsBatch(wmsAuditOutOld.getGoodsBatch());
            wmsStockOut.setKwCode(wmsAuditOutOld.getKwCode());
            wmsStockOut.setCusCode(wmsAuditOutOld.getCusCode());
            wmsStockOut.setDownStatus(CommonWms.FINISH_SHELF_OFF);
            wmsStockOut.setGoodsName(wmsAuditOutOld.getGoodsName());
            wmsStockOut.setCusName(wmsAuditOutOld.getCusName());
            wmsStockOut.setOrderNum(wmsAuditOutOld.getOrderNum());
            wmsStockOut.setBinCode(wmsAuditOutOld.getBinCode());
            wmsStockOut.setTrayCode(wmsAuditOutOld.getTrayCode());
            wmsStockOutMapper.insert(wmsStockOut);

            if (!wmsAuditOutOld.getShelfOkQua().equals(goodsQua)) {
                //下架中
                wmsAuditOutOld.setOrderStatus(CommonWms.BIGIN_SHELF_OFF);
                WmsOutOrderLine wmsOutOrderLine = wmsOutOrderLineMapper.selectById(wmsAuditOutOld.getLineOrderId());
                wmsOutOrderLine.setOrderLineStatus(CommonWms.BIGIN_SHELF_OFF);
                wmsOutOrderLineMapper.updateById(wmsOutOrderLine);

                WmsOutOrderHeader wmsOutOrderHeader = wmsOutOrderHeaderMapper.selectOne(new LambdaQueryWrapper<WmsOutOrderHeader>().eq(WmsOutOrderHeader::getOrderNum, wmsOutOrderLine.getOrderNum()));
                wmsOutOrderHeader.setOrderStatus(CommonWms.BIGIN_SHELF_OFF);
                wmsOutOrderHeaderMapper.updateById(wmsOutOrderHeader);
            } else {
                this.statusChange(wmsAuditOutOld,CommonWms.FINISH_SHELF_OFF);
            }
            //下架
            this.baseMapper.updateById(wmsAuditOutOld);
        }else {
            //库存不够扣情况   库存有多少就扣多少
            wmsAuditOutOld.setShelfOkQua(Convert.toStr(NumberUtil.add(wmsInventories.getBaseCount(), wmsAuditOutOld.getShelfOkQua())));
            wmsAuditOutOld.setShelfSyQua(Convert.toStr(NumberUtil.sub(goodsQua, wmsAuditOutOld.getShelfOkQua())));

            BigDecimal sub = NumberUtil.sub(shelfOffQua,wmsInventories.getBaseCount());
            this.statusChange(wmsAuditOutOld,CommonWms.OUT_OF_STOCK);
            wmsAuditOutOld.setOutStockQua(Convert.toStr(sub));
            this.baseMapper.updateById(wmsAuditOutOld);

            //生成下架信息
            WmsStockOut wmsStockOut = new WmsStockOut();
            wmsStockOut.setGoodsCode(wmsAuditOutOld.getGoodsCode());
            wmsStockOut.setGoodsQua(wmsInventories.getBaseCount());
            wmsStockOut.setAuditId(wmsAuditOutOld.getId());
            wmsStockOut.setGoodsUnitCode(wmsAuditOutOld.getGoodsUnitCode());
            wmsStockOut.setGoodsUnitName(wmsAuditOutOld.getGoodsUnitName());
            wmsStockOut.setGoodsProDate(wmsAuditOutOld.getGoodsProDate());
            wmsStockOut.setGoodsBatch(wmsAuditOutOld.getGoodsBatch());
            wmsStockOut.setKwCode(wmsAuditOutOld.getKwCode());
            wmsStockOut.setCusCode(wmsAuditOutOld.getCusCode());
            wmsStockOut.setDownStatus(CommonWms.FINISH_SHELF_OFF);
            wmsStockOut.setGoodsName(wmsAuditOutOld.getGoodsName());
            wmsStockOut.setCusName(wmsAuditOutOld.getCusName());
            wmsStockOut.setOrderNum(wmsAuditOutOld.getOrderNum());
            wmsStockOut.setBinCode(wmsAuditOutOld.getBinCode());
            wmsStockOut.setTrayCode(wmsAuditOutOld.getTrayCode());
            wmsStockOutMapper.insert(wmsStockOut);

            wmsInventories.setBaseCount("0");
            wmsInventoryMapper.updateById(wmsInventories);
        }
    }

    public void statusChange(WmsAuditOut wmsAuditOutOld,String status){
        wmsAuditOutOld.setOrderStatus(status);
        WmsOutOrderLine wmsOutOrderLine = wmsOutOrderLineMapper.selectById(wmsAuditOutOld.getLineOrderId());
        wmsOutOrderLine.setOrderLineStatus(status);
        wmsOutOrderLineMapper.updateById(wmsOutOrderLine);

        WmsOutOrderHeader wmsOutOrderHeader = wmsOutOrderHeaderMapper.selectOne(new LambdaQueryWrapper<WmsOutOrderHeader>().eq(WmsOutOrderHeader::getOrderNum, wmsOutOrderLine.getOrderNum()));
        if (wmsOutOrderHeader != null) {
            List<WmsOutOrderLine> orderLines = wmsOutOrderLineMapper.selectList(Wrappers.<WmsOutOrderLine>lambdaQuery()
                    .eq(WmsOutOrderLine::getOrderNum, wmsAuditOutOld.getOrderNum()));
            List<WmsOutOrderLine> receivedList = orderLines.stream().filter(line -> line.getOrderLineStatus().equals(status)).collect(Collectors.toList());
            if (orderLines.size() == receivedList.size()) {
                wmsOutOrderHeader.setOrderStatus(status);
                wmsOutOrderHeaderMapper.updateById(wmsOutOrderHeader);
            }
        }
    }
}
