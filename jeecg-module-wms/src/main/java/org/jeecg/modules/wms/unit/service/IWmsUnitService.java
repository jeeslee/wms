package org.jeecg.modules.wms.unit.service;

import org.jeecg.modules.wms.unit.entity.WmsUnit;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 单位
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface IWmsUnitService extends IService<WmsUnit> {

}
