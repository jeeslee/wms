package org.jeecg.modules.wms.outOrder.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.wms.common.constant.CommonWms;
import org.jeecg.modules.wms.outOrder.entity.WmsOutOrderLine;
import org.jeecg.modules.wms.outOrder.service.IWmsOutOrderLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

 /**
 * @Description: 出库单子表
 * @Author: jeecg-boot
 * @Date:   2023-12-06
 * @Version: V1.0
 */
@Api(tags="出库单子表")
@RestController
@RequestMapping("/outOrderLine/wmsOutOrderLine")
@Slf4j
public class WmsOutOrderLineController extends JeecgController<WmsOutOrderLine, IWmsOutOrderLineService> {
	@Autowired
	private IWmsOutOrderLineService wmsOutOrderLineService;
	
	/**
	 * 分页列表查询
	 *
	 * @param wmsOutOrderLine
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "出库单子表-分页列表查询")
	@ApiOperation(value="出库单子表-分页列表查询", notes="出库单子表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<WmsOutOrderLine>> queryPageList(WmsOutOrderLine wmsOutOrderLine,
														@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
														@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
														HttpServletRequest req) {
		QueryWrapper<WmsOutOrderLine> queryWrapper = QueryGenerator.initQueryWrapper(wmsOutOrderLine, req.getParameterMap());
		queryWrapper.eq("order_line_status", CommonWms.WAIT_FOR_OUT);
		Page<WmsOutOrderLine> page = new Page<WmsOutOrderLine>(pageNo, pageSize);
		IPage<WmsOutOrderLine> pageList = wmsOutOrderLineService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param wmsOutOrderLine
	 * @return
	 */
	@AutoLog(value = "出库单子表-添加")
	@ApiOperation(value="出库单子表-添加", notes="出库单子表-添加")
	@RequiresPermissions("outOrderLine:wms_out_order_line:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody WmsOutOrderLine wmsOutOrderLine) {
		wmsOutOrderLineService.save(wmsOutOrderLine);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  任务确认
	 *
	 * @param wmsOutOrderLine
	 * @return
	 */
	@AutoLog(value = "出库单子表-任务确认")
	@ApiOperation(value="出库单子表-任务确认", notes="出库单子表-任务确认")
	@RequiresPermissions("outOrderLine:wms_out_order_line:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody WmsOutOrderLine wmsOutOrderLine) {
		wmsOutOrderLineService.confirmTask(wmsOutOrderLine);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "出库单子表-通过id删除")
	@ApiOperation(value="出库单子表-通过id删除", notes="出库单子表-通过id删除")
	@RequiresPermissions("outOrderLine:wms_out_order_line:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		wmsOutOrderLineService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "出库单子表-批量删除")
	@ApiOperation(value="出库单子表-批量删除", notes="出库单子表-批量删除")
	@RequiresPermissions("outOrderLine:wms_out_order_line:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.wmsOutOrderLineService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "出库单子表-通过id查询")
	@ApiOperation(value="出库单子表-通过id查询", notes="出库单子表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<WmsOutOrderLine> queryById(@RequestParam(name="id",required=true) String id) {
		WmsOutOrderLine wmsOutOrderLine = wmsOutOrderLineService.getById(id);
		if(wmsOutOrderLine==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(wmsOutOrderLine);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param wmsOutOrderLine
    */
    @RequiresPermissions("outOrderLine:wms_out_order_line:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, WmsOutOrderLine wmsOutOrderLine) {
        return super.exportXls(request, wmsOutOrderLine, WmsOutOrderLine.class, "出库单子表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequiresPermissions("outOrderLine:wms_out_order_line:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, WmsOutOrderLine.class);
    }

}
