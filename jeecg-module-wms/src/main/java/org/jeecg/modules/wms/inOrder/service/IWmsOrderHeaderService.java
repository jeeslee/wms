package org.jeecg.modules.wms.inOrder.service;

import org.jeecg.modules.wms.inOrder.entity.WmsOrderLine;
import org.jeecg.modules.wms.inOrder.entity.WmsOrderHeader;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 入库单主表
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface IWmsOrderHeaderService extends IService<WmsOrderHeader> {

	/**
	 * 添加一对多
	 *
	 * @param wmsOrderHeader
	 * @param wmsOrderLineList
	 */
	public void saveMain(WmsOrderHeader wmsOrderHeader, List<WmsOrderLine> wmsOrderLineList) ;

	/**
	 * 修改一对多
	 *
	 * @param wmsOrderHeader
	 * @param wmsOrderLineList
	 */
	public void updateMain(WmsOrderHeader wmsOrderHeader, List<WmsOrderLine> wmsOrderLineList);
	
	/**
	 * 删除一对多
	 *
	 * @param id
	 */
	public void delMain(String id);
	
	/**
	 * 批量删除一对多
	 *
	 * @param idList
	 */
	public void delBatchMain(Collection<? extends Serializable> idList);

	/**
	 * 自定义添加逻辑
	 * @param wmsOrderHeader
	 * @param wmsOrderLineList
	 */
	public void saveWmsOrderHeader(WmsOrderHeader wmsOrderHeader, List<WmsOrderLine> wmsOrderLineList);

}
