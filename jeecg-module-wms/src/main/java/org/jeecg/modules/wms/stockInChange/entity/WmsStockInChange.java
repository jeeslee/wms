package org.jeecg.modules.wms.stockInChange.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 上架调整记录表
 * @Author: jeecg-boot
 * @Date:   2023-11-28
 * @Version: V1.0
 */
@Data
@TableName("wms_stock_in_change")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="wms_stock_in_change对象", description="上架调整记录表")
public class WmsStockInChange implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**物料编码*/
	@Excel(name = "物料编码", width = 15)
    @ApiModelProperty(value = "物料编码")
    private String goodsCode;
	/**物料名称*/
	@Excel(name = "物料名称", width = 15)
    @ApiModelProperty(value = "物料名称")
    private String goodsName;
	/**数量(旧)*/
	@Excel(name = "数量(旧)", width = 15)
    @ApiModelProperty(value = "数量(旧)")
    private String shelfOldCount;
	/**数量(新)*/
	@Excel(name = "数量(新)", width = 15)
    @ApiModelProperty(value = "数量(新)")
    private String shelfNewCount;
	/**单据号*/
	@Excel(name = "单据号", width = 15)
    @ApiModelProperty(value = "单据号")
    private String orderNum;
	/**标签号(旧)*/
	@Excel(name = "标签号(旧)", width = 15)
    @ApiModelProperty(value = "标签号(旧)")
    private String oldBinCode;
	/**标签号(新)*/
	@Excel(name = "标签号(新)", width = 15)
    @ApiModelProperty(value = "标签号(新)")
    private String newBinCode;
	/**托盘码(旧)*/
	@Excel(name = "托盘码(旧)", width = 15)
    @ApiModelProperty(value = "托盘码(旧)")
    private String oldTrayCode;
	/**托盘码(新)*/
	@Excel(name = "托盘码(新)", width = 15)
    @ApiModelProperty(value = "托盘码(新)")
    private String newTrayCode;
	/**库位(旧)*/
	@Excel(name = "库位(旧)", width = 15)
    @ApiModelProperty(value = "库位(旧)")
    private String oldKwCode;
	/**库位(新)*/
	@Excel(name = "库位(新)", width = 15)
    @ApiModelProperty(value = "库位(新)")
    private String newKwCode;
    /**来源ID*/
//    @Excel(name = "库位(新)", width = 15)
    @ApiModelProperty(value = "来源ID")
    private String sourceId;
    /**备注*/
    @Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private String remark;
}
