package org.jeecg.modules.wms.outOrder.mapper;

import java.util.List;
import org.jeecg.modules.wms.outOrder.entity.WmsOutOrderLine;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 出库单子表
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface WmsOutOrderLineMapper extends BaseMapper<WmsOutOrderLine> {

	/**
	 * 通过主表id删除子表数据
	 *
	 * @param mainId 主表id
	 * @return boolean
	 */
	public boolean deleteByMainId(@Param("mainId") String mainId);

  /**
   * 通过主表id查询子表数据
   *
   * @param mainId 主表id
   * @return List<WmsOutOrderLine>
   */
	public List<WmsOutOrderLine> selectByMainId(@Param("mainId") String mainId);
}
