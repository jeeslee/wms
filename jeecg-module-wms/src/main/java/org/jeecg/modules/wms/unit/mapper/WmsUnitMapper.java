package org.jeecg.modules.wms.unit.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.wms.unit.entity.WmsUnit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 单位
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface WmsUnitMapper extends BaseMapper<WmsUnit> {

}
