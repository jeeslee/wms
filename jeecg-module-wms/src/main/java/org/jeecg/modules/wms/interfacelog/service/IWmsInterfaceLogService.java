package org.jeecg.modules.wms.interfacelog.service;

import org.jeecg.modules.wms.interfacelog.entity.WmsInterfaceLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 接口日志表
 * @Author: jeecg-boot
 * @Date:   2024-01-10
 * @Version: V1.0
 */
public interface IWmsInterfaceLogService extends IService<WmsInterfaceLog> {

}
