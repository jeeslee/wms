package org.jeecg.modules.wms.inOrder.vo;

import java.util.List;
import org.jeecg.modules.wms.inOrder.entity.WmsOrderHeader;
import org.jeecg.modules.wms.inOrder.entity.WmsOrderLine;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 入库单主表
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
@Data
@ApiModel(value="wms_order_headerPage对象", description="入库单主表")
public class WmsOrderHeaderPage {

	/**id*/
	@ApiModelProperty(value = "id")
    private String id;
	/**创建人登录名称*/
	@ApiModelProperty(value = "创建人登录名称")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人登录名称*/
	@ApiModelProperty(value = "更新人登录名称")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**所属公司*/
	@Excel(name = "所属公司", width = 15)
	@ApiModelProperty(value = "所属公司")
    private String sysCompanyCode;
	/**客户编码*/
	@Excel(name = "客户编码", width = 15)
	@ApiModelProperty(value = "客户编码")
    private String cusCode;
	/**预计到货时间*/
	@Excel(name = "预计到货时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "预计到货时间")
    private Date arrivalDate;
	/**司机*/
	@Excel(name = "司机", width = 15)
	@ApiModelProperty(value = "司机")
    private String carDriver;
	/**订单类型*/
	@Excel(name = "订单类型编码", width = 15)
	@ApiModelProperty(value = "订单类型编码")
    private String orderTypeCode;
	/**订单类型名称*/
	@Excel(name = "订单类型名称", width = 15)
	@ApiModelProperty(value = "订单类型名称")
	private String orderTypeName;
	/**单据状态*/
	@Excel(name = "单据状态", width = 15)
	@ApiModelProperty(value = "单据状态")
    private String orderStatus;
	/**单号*/
	@Excel(name = "单号", width = 15)
	@ApiModelProperty(value = "单号")
    private String orderNum;
	/**供应商编码*/
	@Excel(name = "供应商编码", width = 15)
	@ApiModelProperty(value = "供应商编码")
    private String supCode;
	/**供应商名称*/
	@Excel(name = "供应商名称", width = 15)
	@ApiModelProperty(value = "供应商名称")
    private String supName;
	/**客户名称*/
	@Excel(name = "客户名称", width = 15)
	@ApiModelProperty(value = "客户名称")
    private String cusName;
	/**仓库代码*/
	@Excel(name = "仓库代码", width = 15)
	@ApiModelProperty(value = "仓库代码")
    private String warehouseCode;
	/**仓库名称*/
	@Excel(name = "仓库名称", width = 15)
	@ApiModelProperty(value = "仓库名称")
    private String warehouseName;
	/**租户ID*/
	@Excel(name = "租户ID", width = 15)
	@ApiModelProperty(value = "租户ID")
    private Integer tenantId;
	/**备注*/
	@Excel(name = "备注", width = 15)
	@ApiModelProperty(value = "备注")
    private String remark;
	/**删除状态(0-正常,1-已删除)*/
	@Excel(name = "删除状态(0-正常,1-已删除)", width = 15)
	@ApiModelProperty(value = "删除状态(0-正常,1-已删除)")
    private Integer delFlag;
	
	@ExcelCollection(name="入库单子表")
	@ApiModelProperty(value = "入库单子表")
	private List<WmsOrderLine> wmsOrderLineList;
	
}
