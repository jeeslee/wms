package org.jeecg.modules.wms.interfacelog.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.wms.interfacelog.entity.WmsInterfaceLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 接口日志表
 * @Author: jeecg-boot
 * @Date:   2024-01-10
 * @Version: V1.0
 */
public interface WmsInterfaceLogMapper extends BaseMapper<WmsInterfaceLog> {

}
