package org.jeecg.modules.wms.kw.service;

import org.jeecg.modules.wms.kw.entity.WmsKw;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 库位
 * @Author: jeecg-boot
 * @Date:   2023-11-13
 * @Version: V1.0
 */
public interface IWmsKwService extends IService<WmsKw> {

}
