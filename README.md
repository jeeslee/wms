仓储管理系统（WMS）
===============

当前最新版本： 1.0.0（持续更新:敬请关注:合作加V） 

![输入图片说明](https://gitee.com/jeeslee/wms/raw/master/合作加我.png "订单类型")

项目介绍
-----------------------------------
WMS是仓库管理系统(Warehouse Management System) 的缩写，是实现产品批次整理、库存盘点以及质检等程序一体化管理的系统，可以有效地对仓库业务进行全方位的控制和跟踪，WMS中的软件指的是支持整个系统运作的软件部分，包括收货处理、上架管理、拣货作业、月台管理、补货管理、库内作业、越库操作、循环盘点、RF操作、加工管理、矩阵式收费等。仓储管理系统中的硬件指的是用于打破传统数据采集和上传的瓶颈问题，利用自动识别技术和无线传输提高数据的精度和传输的速度。管理经验指的是开发商根据其开发经验中客户的管理方式和理念整合的一套管理理念和流程，为企业做到真正的管理！


适用场景
-----------------------------------
* 电子商务类存放仓库: 电子商务平台，以存放仓库为主体的京东商城亦或亚马逊服务平台都所采用的wms系统。电子商务平台十分重视仓库管理与配送管理，二者乃至取决于平台上的生死攸关。电子商务平台一定要借助系统的巨大作用，除开库存量存储系统外，也包含在线客服等在内的一体化供应链管理，才能够让全部电子商务平台有效标准化的运行。大家都知道，电商平台货物积存量也较大，要是没有一个完善的系统去有效记录货物动向及其库存量动态性，那就表明这样的平台会有杂乱的情况，就意味着这样的平台随时都可能发生可燃性的困境。
* 大中型存放仓库: 很多大中型存放仓库，一般储放一些大型钢材、滑轨、装饰建材、室内装修材料等，货物是来自于不一样生产厂家、企业，因此需要很明确的区别开来、并且货物交货总数及其销售量一不小心就会会出现变化，为了把不一样商户的货物确立区别，就需要采用wms系统。大中型仓库储放商品的时间不确定性的，并且货物也并不是一次性的就取下，假如不能使用wms系统则往往会在中间商出问题。
* 物流公司: 大中型物流公司，都一定要应用技术专业仓储系统。物流公司每日货物出入成千上万，交货、拿货总数及其日期一定要有一定的操控，因此应用wms系统确实十分便捷。物流公司使用wms系统以前，需要花费大量人力、物力资源，才能保持物流公司的正常运作工作中，并且人力资源终究比不上电子技术那么准确。



项目源码
-----------------------------------
| 仓库 |前端源码 Vue3版 | 后端JAVA源码 |
|-|-|-|
| 码云 | [wms-vue3](https://gitee.com/li_tongs/jeecgboot-vue3?_from=gitee_search)   | [wms](https://gitee.com/jeeslee/wms?_from=gitee_search) |


#### 项目说明

| 项目名                | 说明                     | 
|--------------------|------------------------|
| `wms`    | JAVA后台源码（支持微服务）        |
| `wms-vue3` | 前端源码 (Vue3版本) |


技术架构：
-----------------------------------
#### 开发环境

- 语言：Java 8+ (小于17)

- IDE(JAVA)： IDEA (必须安装lombok插件 )

- IDE(前端)： Vscode、WebStorm、IDEA

- 依赖管理：Maven

- 缓存：Redis

- 数据库脚本：MySQL5.7+  &  Oracle 11g & Sqlserver2017


#### 后端

- 基础框架：Spring Boot 2.6.14

- 微服务框架： Spring Cloud Alibaba 2021.0.1.0

- 持久层框架：MybatisPlus 3.5.1

- 报表工具： JimuReport 1.5.8

- 安全框架：Apache Shiro 1.10.0，Jwt 3.11.0

- 微服务技术栈：Spring Cloud Alibaba、Nacos、Gateway、Sentinel、Skywalking

- 数据库连接池：阿里巴巴Druid 1.1.22

- 日志打印：logback

- 其他：autopoi, fastjson，poi，Swagger-ui，quartz, lombok（简化代码）等。


#### 前端

- Vue3版本：`Vue3.0+TypeScript+Vite+AntDesignVue+pinia+echarts等新方案` [详细查看](https://github.com/jeecgboot/jeecgboot-vue3)

#### 支持库

|  数据库   |  支持   |
| --- | --- |
|   MySQL   |  √   |
|  Oracle11g   |  √   |
|  Sqlserver2017   |  √   |
|   PostgreSQL   |  √   |
|   MariaDB   |  √   |
|   达梦、人大金仓   |  √   |



## 微服务解决方案

1、服务注册和发现 Nacos √

2、统一配置中心 Nacos  √

3、路由网关 gateway(三种加载方式) √

4、分布式 http feign √

5、熔断降级限流 Sentinel √

6、分布式文件 Minio、阿里OSS √ 

7、统一权限控制 JWT + Shiro √

8、服务监控 SpringBootAdmin√

9、链路跟踪 Skywalking   [参考文档](https://help.jeecg.com/java/springcloud/super/skywarking.html)

10、消息中间件 RabbitMQ  √

11、分布式任务 xxl-job  √ 

12、分布式事务 Seata

13、分布式日志 elk + kafka

14、支持 docker-compose、k8s、jenkins

15、CAS 单点登录   √

16、路由限流   √


### 功能模块
```
├─系统管理
│  ├─用户管理
│  ├─角色管理
│  ├─菜单管理
│  ├─权限设置（支持按钮权限、数据权限）
│  ├─表单权限（控制字段禁用、隐藏）
│  ├─部门管理
│  ├─我的部门（二级管理员）
│  └─字典管理
│  └─分类字典
│  └─系统公告
│  └─职务管理
│  └─通讯录
│  └─多租户管理
├─消息中心
│  ├─消息管理
│  ├─模板管理
├─代码生成器(低代码)
│  ├─代码生成器功能（一键生成前后端代码，生成后无需修改直接用，绝对是后端开发福音）
│  ├─代码生成器模板（提供4套模板，分别支持单表和一对多模型，不同风格选择）
│  ├─代码生成器模板（生成代码，自带excel导入导出）
│  ├─查询过滤器（查询逻辑无需编码，系统根据页面配置自动生成）
│  ├─高级查询器（弹窗自动组合查询条件）
│  ├─Excel导入导出工具集成（支持单表，一对多 导入导出）
│  ├─平台移动自适应支持
├─系统监控
│  ├─Gateway路由网关
│  ├─性能扫描监控
│  │  ├─监控 Redis
│  │  ├─Tomcat
│  │  ├─jvm
│  │  ├─服务器信息
│  │  ├─请求追踪
│  │  ├─磁盘监控
│  ├─定时任务
│  ├─系统日志
│  ├─消息中心（支持短信、邮件、微信推送等等）
│  ├─数据日志（记录数据快照，可对比快照，查看数据变更情况）
│  ├─系统通知
│  ├─SQL监控
│  ├─swagger-ui(在线接口文档)
└─其他模块
   └─更多功能开发中。。
   
```

### 系统效果

##### PC端

![](https://oscimg.oschina.net/oscnet/up-000530d95df337b43089ac77e562494f454.png)

##### 供应商

![输入图片说明](https://gitee.com/jeeslee/wms/raw/master/suply.png "供应商")

##### 物料

![输入图片说明](https://gitee.com/jeeslee/wms/raw/master/goods.png "物料")

##### 仓库

![输入图片说明](https://gitee.com/jeeslee/wms/raw/master/warehouse.png "仓库")

##### 订单类型

![输入图片说明](https://gitee.com/jeeslee/wms/raw/master/orderType.png "订单类型")

##### 入库

![输入图片说明](https://gitee.com/jeeslee/wms/raw/master/orderIn.png "入库")

##### 质检

![输入图片说明](https://gitee.com/jeeslee/wms/raw/master/qc.png "质检")

##### 质检明细

![输入图片说明](https://gitee.com/jeeslee/wms/raw/master/qcDetail.png "质检明细")

##### 上架

![输入图片说明](https://gitee.com/jeeslee/wms/raw/master/onShelf.png "上架")
![输入图片说明](https://gitee.com/jeeslee/wms/raw/master/onShelfDe.png "上架")
![输入图片说明](https://gitee.com/jeeslee/wms/raw/master/onShelfDe2.png "上架")
![输入图片说明](https://gitee.com/jeeslee/wms/raw/master/onShelfDe3.png "上架")

##### 出库

![输入图片说明](https://gitee.com/jeeslee/wms/raw/master/orderOut.png "出库")
![输入图片说明](https://gitee.com/jeeslee/wms/raw/master/orderOutTask.png "任务确认")

##### 库存标签

![输入图片说明](https://gitee.com/jeeslee/wms/raw/master/kcbz.png "库存标签")

##### 欠货列表
![输入图片说明](https://gitee.com/jeeslee/wms/raw/master/qhlb.png "欠货列表")

##### 盘点列表
![输入图片说明](https://gitee.com/jeeslee/wms/raw/master/pd.png "盘点列表")

##### 库内管理

![输入图片说明](https://gitee.com/jeeslee/wms/raw/master/bqzy.png "标签转移")
![输入图片说明](https://gitee.com/jeeslee/wms/raw/master/kwzy.png "库位转移")
![输入图片说明](https://gitee.com/jeeslee/wms/raw/master/inventory.png "库存")

![](https://oscimg.oschina.net/oscnet/up-9d6f36f251e71a0b515a01323474b03004c.png)

![输入图片说明](https://static.oschina.net/uploads/img/201904/14160813_KmXS.png "在这里输入图片标题")

![输入图片说明](https://static.oschina.net/uploads/img/201904/14160935_Nibs.png "在这里输入图片标题")

![输入图片说明](https://static.oschina.net/uploads/img/201904/14161004_bxQ4.png "在这里输入图片标题")

#####  系统交互
![](https://oscimg.oschina.net/oscnet/up-78b151fc888d4319377bf1cc311fe826871.png)

![](https://oscimg.oschina.net/oscnet/up-16c07e000278329b69b228ae3189814b8e9.png)


##### 报表设计器
![](https://oscimg.oschina.net/oscnet/up-64648de000851f15f6c7b9573d107ebb5f8.png)

![](https://oscimg.oschina.net/oscnet/up-fa52b44445db281c51d3f267dce7450d21b.gif)

![](https://oscimg.oschina.net/oscnet/up-68a19149d640f1646c8ed89ed4375e3326c.png)

![](https://oscimg.oschina.net/oscnet/up-f7e9cb2e3740f2d19ff63b40ec2dd554f96.png)

##### 表单设计器
![](https://oscimg.oschina.net/oscnet/up-5f8cb657615714b02190b355e59f60c5937.png)

![](https://oscimg.oschina.net/oscnet/up-d9659b2f324e33218476ec98c9b400e6508.png)

![](https://oscimg.oschina.net/oscnet/up-4868615395272d3206dbb960ade02dbc291.png)
